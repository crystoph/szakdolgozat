import express from 'express'
import cors from 'cors';
import mongoose from 'mongoose'
import { load } from 'ts-dotenv';
import { decodeToken } from './Helpers/Auth';
import { userRouter } from './Routes/UserRouter';
import newsRouter from './Routes/NewsRouter';
import courseRouter from './Routes/CourseRouter';
import { errorHandler, logger, outPutLogger } from './Middlewares/LoggingMiddleware';
import quizzRouter from './Routes/QuizzRouter';
import schoolRouter from './Routes/SchoolRouter';
import fileUpload from 'express-fileupload';

const app = express();
const env = load({
    JWT_SECRET_TOKEN: String,
    JWT_REFRESH_TOKEN: String,
    DATABASE: String,
    API_PORT: Number,
});

app.use(cors());
app.use(express.json({limit: '10mb'}));
app.use(logger);
app.use(decodeToken);
app.use(fileUpload({
    createParentPath: true,
    // limits: { 
    //     fileSize: 2 * 1024 * 1024 * 1024 
    // },
}));
// app.use(outPutLogger);

app.get('/', (req, res) => {
    res.send('Private LMS API');
});

app.use('/schools', schoolRouter);
app.use('/users', userRouter);
app.use('/news', newsRouter);
app.use('/courses', courseRouter);
app.use('/quizzes', quizzRouter);
app.use(errorHandler);

app.listen(env.API_PORT, async () => {
    await mongoose.connect(env.DATABASE)
        .catch(err => console.log('No connection with db!', err))
        .then(() => console.log('DB connected!'));

    console.log('App server active on port ' + env.API_PORT);
});

export default app;