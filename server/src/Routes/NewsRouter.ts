import express from "express";
import News from '../Models/Schemas/News';
import { isAdmin } from '../Middlewares/GuardMiddlewares';
import User from "../Models/Schemas/User";
import School from "../Models/Schemas/School";
import { getUserIdFromToken } from "../Helpers/Auth";
import mongoose from "mongoose";

const newsRouter = express.Router();

newsRouter.get('', async (req, res, next) => {
    try{
        const schoolId = req.query.schoolId;
        if (!schoolId){
            return res.sendStatus(400);
        }
        const school = await School.findById(schoolId);
        if(!school){
            return res.status(404);
        }
        let result = [];
        if (req.query.id){
            const id = req.query.id;
            result = await News.find({school: school._id, _id: id})
                .populate('createdBy', 'name id profilePicture')
                .sort({publishedAt: 'desc'});
        }else {
            const userId = await getUserIdFromToken(req);
            if (userId){
                const userObj = new mongoose.Types.ObjectId(userId);
                result = await News.find({school: school._id, 
                    $or: [{isPublic: true}, {createdBy: userObj}]
                })
                    .populate('createdBy', 'name id')
                    .sort({publishedAt: 'desc'});
            }else {
                result = await News.find({school: school._id, isPublic: true})
                    .populate('createdBy', 'name, id')
                    .sort({publishedAt: 'desc'});
            }
        }
        res.json(result);
    }catch(err){
        console.log(err);
        next(err);
    }
});

newsRouter.post('', isAdmin, async (req: any, res) => {
    const userId = req.decodedToken._id;
    const user = await User.findById(userId);
    const n = await News.create({
        title: req.body.title,
        description: req.body.description,
        school: user?.school,
        createdBy: user?._id,
    });
    res.json(n);
});

newsRouter.delete('/:id', isAdmin, async (req, res) => {
    await News.deleteOne({ _id: req.params.id });
    console.log(`DELETED ${req.params.id}!`);
    res.json({ status: 'Success' });
});

newsRouter.put('/:id', async (req, res) => {
    await News.updateOne({ _id: req.params.id }, {
        title: req.body.title,
        description: req.body.description,
    });
    res.json({ status: 'Success' });
});

newsRouter.put('/:id/publish', async (req, res) => {
    const news = await News.findOne({ _id: req.params.id });
    if (news.isPublic === false && req.body.isPublic === true){ 
        const now = new Date();
        req.body.publishedAt = now;
    }
    if (news.isPublic === true && req.body.isPublic === false){
        req.body.publishedAt = undefined;
    }
    await News.updateOne({ _id: req.params.id }, {
        isPublic: req.body.isPublic,
        publishedAt: req.body.publishedAt,
    });
    news.isPublic = req.body.isPublic;
    res.json(news);
});

export default newsRouter;