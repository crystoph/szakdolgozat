import express from "express";
import User from "../Models/Schemas/User";
import validate from "../Middlewares/ValidationMiddleware";
import bcrypt from 'bcrypt';
import { UserType } from "../Models/Schemas/enums/Enums";
import School from "../Models/Schemas/School";
import { schoolDataValidator, schoolIdValidator } from "../Models/Validators/SchoolValidator";
import { userDataValidator, userPasswordValidator } from "../Models/Validators/UserValidator";
import { isAdmin, isSchoolVisible } from "../Middlewares/GuardMiddlewares";
import { load } from "ts-dotenv";

const schoolRouter = express.Router();

const env = load({
    SALT_ROUNDS: Number,
 });

schoolRouter.post('', 
    validate([...schoolDataValidator, ...userDataValidator, ...userPasswordValidator]),
    async (req:any, res:any) => {
        const u = await User.create({
            name: req.body.userName.trim(),
            email: req.body.email,
            password: await bcrypt.hash(req.body.password, env.SALT_ROUNDS),
            userType: UserType.ADMIN,
            positionType: 'Adminisztrátor',
        });

        const schoolName:string = req.body.schoolName.trim();
        const school = await School.create({
            name: schoolName,
            pageUrl: encodeURIComponent(schoolName),
            users: [u._id],
            emblem:''
        });
        await User.updateOne({_id: u._id}, { school: school._id}).exec();

        res.status(201).json({
            success: true,
            message: 'Sikeresen létre hozta iskoláját!',
            schoolName: school.name,
            schoolId: school._id,
            emblem: school.emblem,
        });
    }
);

schoolRouter.put('/:schoolId', 
    validate([...schoolIdValidator, ...schoolDataValidator]),
    async (req:any, res:any) => {
        await School.updateOne({_id: req.body._id}, 
            {
                name: req.body.schoolName,
                pageUrl: req.body.pageUrl,
            });
        const school = await School.findById(req.body._id);
        if (!school){
            return res.status(404).send();
        }
        res.json({
            schoolName: school.name,
            pageUrl: encodeURIComponent(school.pageUrl),
            _id: school._id,
        });
    }
);

schoolRouter.get('/:schoolId',
    isSchoolVisible,
    validate(schoolIdValidator),
    async (req:any, res:any) => {
        const school = await School.findOne({_id: req.params.schoolId}, '_id name pageUrl emblem');
        res.json({
            schoolName: school?.name,
            pageUrl: school?.pageUrl,
            _id: school?._id,
            emblem: school?.emblem,
        });
    }
);

schoolRouter.get('', async (req:any, res:any) => {
    const url = req.query.url;
    if (!url){
        return res.status(400);
    }
    const school = await School.findOne({pageUrl: url});
    if (!school){
        return res.sendStatus(404);
    }
    res.json({
        _id: school._id,
        schoolName: school.name,
        emblem: school.emblem,
    });
});

schoolRouter.post('/:schoolId/emblem',
    isAdmin,
    async (req:any, res) => {
        const base64 = req.body.emblemBase64;
        const schoolId = req.params.schoolId;
        await School.updateOne({_id:schoolId}, { emblem: base64 });
        res.json({success: true});
    }
);

export default schoolRouter;