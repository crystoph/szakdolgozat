import express from "express";
import User from '../Models/Schemas/User';
import { UserType } from "../Models/Schemas/enums/Enums";
import { load } from "ts-dotenv";
import bcrypt from 'bcrypt';
import { userDataValidator, userIdValidator, userPasswordValidator } from "../Models/Validators/UserValidator";
import validate from "../Middlewares/ValidationMiddleware";
import { isAdmin } from "../Middlewares/GuardMiddlewares";
import { defaultUserLenghts } from "../Helpers/Constants";
import { makeRandomStr } from "../Helpers/HelperFuntions";
import School from "../Models/Schemas/School";
import mongoose from "mongoose";

export const userRouter = express.Router();

const env = load({
   SALT_ROUNDS: Number, 
});

userRouter.get('/', 
    isAdmin,
    async (req, res) => {
        const schoolId = req.query.schoolId;    
        const users = await User.find({ school: schoolId }, 'name email _id isDefaultPassword').exec()
        res.json(users);
    }
);

userRouter.get('/:id', 
    async (req, res) => {
        const user = await User.findOne({ _id: req.params.id });
            // .populate('school');
            const school = await School.findById(user?.school);
        res.json({
            name: user?.name,
            schoolName: school?.name,
            // positionType: user?.positionType,
            _id: user?._id,
            email: user?.email,
            profilePicture: user?.profilePicture,
        });
});

userRouter.delete('/:id', 
    isAdmin,
    async (req, res) => {
        await User.deleteOne({ _id: req.params.id });
        console.log(`DELETED ${req.params.id}!`);

        res.json({ status: 'Success' });
    }
);

userRouter.put('/:userId', 
    validate(userDataValidator), validate(userIdValidator),
    async (req, res) => {
        await User.updateOne({ _id: req.params.userId }, {
            name: req.body.name,
            email: req.body.email,
        });
        const user = await User.findById(req.params.userId);
        res.json({
            name: user?.name,
            email: user?.email,
            _id: user?._id,
        });
});

userRouter.put('/:userId/ChangePassword', 
    validate(userIdValidator), validate(userPasswordValidator),
    async (req:any, res) => {
        if (req.decodedToken._id !== req.params.userId){
            return res.json({success: false, message: 'Csak a saját jelszavát módosíthatja.'});
        }
        await User.updateOne({ _id: req.params.userId }, {
            password: await bcrypt.hash(req.body.password, env.SALT_ROUNDS),
            isDefaultPassword: false,
        });
        res.json({ success: true, message: 'Sikeres jelszó változtatás!'})
});

userRouter.put('/:id/makeAdmin',
    isAdmin, validate(userIdValidator),
    async (req, res) => {
        await User.updateOne({_id:req.params.id}, { userType: UserType.ADMIN});
        res.sendStatus(200)
    }
);

userRouter.put('/:id/makeUser', 
    isAdmin, validate(userIdValidator),
    async (req, res) => {
        await User.updateOne({_id:req.params.id}, { userType: UserType.USER});
        res.sendStatus(200);
    }
);

userRouter.patch('/generate', 
    async (req, res) => {
        const count: number = req.body.numberOfUsers ?? 1;
        const school: mongoose.ObjectId = req.body.schoolId;
        let name: string, password: string, users = [];
        for (let i=0; i<count; i++){
            do{
                name = makeRandomStr(defaultUserLenghts.name);
            }
            while (await User.count({name: name}) !== 0);
            password = makeRandomStr(defaultUserLenghts.password);
            // const password = await bcrypt.hash(passwordStr, env.SALT_ROUNDS);
            users.push({name, password, school, isDefaultPassword: true, profilePicture:''});
        }
        const addedUsers = await User.insertMany(users);
        const addedUserIds = addedUsers.map(u => u._id);
        console.log(addedUsers);
        
        await School.updateMany({ _id: school}, { $push: { users: addedUserIds} });
        console.log(await School.findById(school));
        
        res.json(addedUsers);
    }
);

userRouter.get('/:userId/defaultPassword',
    validate(userIdValidator),
    isAdmin,
    async (req, res) => {
        const user = await User.findOne({ _id: req.params.userId});
        if (!user?.isDefaultPassword)
        {
            console.log('No default password')
            return res.status(400).send('A felhasználó jelszavát nem másolhatja!');
        }
        res.json(user.password);
    }
);

userRouter.post('/:userId/profile',
    validate(userIdValidator),
    async (req:any, res) => {
        const base64 = req.body.profileBase64;
        const userId = req.decodedToken._id;
        await User.updateOne({_id:userId}, { profilePicture:base64 });
        res.json({success: true});
    }
);