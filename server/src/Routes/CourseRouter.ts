import express from "express";
import Course from '../Models/Schemas/Course';
import Quizz from "../Models/Schemas/Quizz";
import validate from "../Middlewares/ValidationMiddleware";
import { quizzIdValidator } from "../Models/Validators/QuizzValidator";
import { courseIdValidator, courseQuizzAssignValidator } from "../Models/Validators/CourseValidator";
import mongoose from 'mongoose';
import { UserRole, UserType } from '../Models/Schemas/enums/Enums';
import User from '../Models/Schemas/User';
import { isAdmin, isAdminOrTeacherOfCourse } from "../Middlewares/GuardMiddlewares";
import Attachment from "../Models/Schemas/Attachment";
import { mkdirSync, existsSync } from "fs";
import UserAnswer from "../Models/Schemas/UserAnswer";
import ICourse from "../Models/Interfaces/ICourse";

const courseRouter = express.Router();

courseRouter.get('/:courseId/quizzes',
    async (req:any, res:any) => {
        const userId = req.decodedToken._id;
        // const user = User.findById(userId);
        const uans = await UserAnswer.find({user: userId});
        const doneQuizzes = uans.map(q => q.quizId);
        const course = await Course.findOne({ _id: req.params.courseId}).populate('quizzes');
        const result = course?.quizzes.map(qui => {
            const res = {...qui, isCompleted: false};
            if (doneQuizzes.includes(qui._id)){
                res.isCompleted = true;
            }
            return res;
        });
        res.json(result);
    });

courseRouter.post('/:courseId/quizz/:quizzId',
    validate([...quizzIdValidator, ...courseIdValidator, ...courseQuizzAssignValidator]),
    async (req:any, res:any) => {
        await Quizz.updateOne({ _id: req.params.quizzId }, { course: req.params.courseId });
        await Course.updateOne({ _id: req.params.courseId }, { $push: { quizzes: req.params.quizzId } });
        const quizz = await Quizz.findOne({ _id: req.params.quizzId });
        res.json(quizz);
    });

courseRouter.get('/', 
    async (req: any, res: any) => {
        const userId = req.decodedToken._id;
        const user = await User.findById(userId).populate('courses').exec();
        // console.log('USER.COURSES', user?.courses);
        const allCourse = await Course.find({school: user?.school});
        // console.log(allCourse);
        const result = user?.userType === UserType.ADMIN ? allCourse : user?.courses;
        res.json(result);
    }
);

courseRouter.post('/', 
    isAdmin,
    async (req: any, res: any) => {
    if (await Course.exists({name:req.body.name})){
        return res.status(409).json({message: 'Ilyen nevű kurzus már létezik!'});
    }
    const user = await User.findById(req.decodedToken._id);
    const course = await Course.create({
        name: req.body.name,
        description: req.body.description,
        school: user?.school,
    });

    res.status(200).json({course});
});

courseRouter.get('/:courseId', async (req: any, res: any) => {
    const _id = req.params.courseId;
    const course = await Course.findOne<ICourse>({_id})
        .populate('teachers students', '_id');
    // const userId: mongoose.ObjectId = req.decodedToken._id;
    // const isTeacher = course?.teachers.some(t => t === userId);

    return res.status(200).json(course);
});

courseRouter.delete('/:courseId', async (req: any, res: any) => {
    const _id = req.params.courseId;
    if (!mongoose.isValidObjectId(_id) || !await Course.exists({_id})){
        return res.sendStatus(404);
    }
    await Course.deleteOne({_id});
    console.log(`DELETED Course: ${_id}!`);
    res.status(200).send({_id});
});

courseRouter.put('/:courseId', async (req: any, res: any) => {
    const _id = req.params.courseId;
    if (!mongoose.isValidObjectId(_id) || !await Course.exists({_id})){
        return res.sendStatus(404);
    }
    const course = await Course.updateOne({_id}, {
        name: req.body.name,
        description: req.body.description,
    });
    console.log(`UPDATED Course: ${_id}!`);

    return res.status(200).json(course);
});

courseRouter.post('/:courseId/addUsers', isAdminOrTeacherOfCourse, 
    async (req:any, res:any) => {
        const students: mongoose.Types.ObjectId[] = req.body.students.map((u:any) => {
            if (mongoose.isValidObjectId(u._id))
                return new mongoose.Types.ObjectId(u._id);
        });
        const teachers: mongoose.Types.ObjectId[] = req.body.teachers.map((u:any) => {
            if (mongoose.isValidObjectId(u._id))
                return new mongoose.Types.ObjectId(u._id);
        });
        const users = Array.prototype.concat(students, teachers);
        const courseId = new mongoose.Types.ObjectId(req.params.courseId);
        await Course.updateOne({ _id: req.params.courseId }, { $push: { students:students, teachers:teachers } });
        await User.updateMany({ _id: { $in: users}}, { $push: { courses: courseId } });

        const results = Array.prototype.concat(req.body.students, req.body.teachers);
        res.json(results);
});

courseRouter.get('/:courseId/usersNotInCourse', isAdminOrTeacherOfCourse,
async (req:any, res:any) => {
    const courseId = req.params.courseId;
    const course = await Course.findById(courseId)
        .populate('students').populate('teachers');
    const userIdsInCourse = mergeCourseUsers(course).map(uc => uc._id);
    const user = await User.findById(req.decodedToken._id);
    const users = (await User.find({ _id: { $nin: userIdsInCourse }, school: user?.school}, {_id:1, name:1}));

    return res.status(200).json(users);
});

const mergeCourseUsers = (course:any) => {
    const arr1 = course?.students.map((s:any) => {
        return {
            userRole: UserRole.STUDENT,
            name: s.name,
            _id: s._id,
        };
    });
    const arr2 = course?.teachers.map((s:any) => {
        return {
            userRole: UserRole.TEACHER,
            name: s.name,
            _id: s._id,
        };
    });
    
    return Array.prototype.concat(arr1, arr2);
}

courseRouter.get('/:courseId/userRole/:userId', async (req: any, res: any) => {
    const courseId = req.params.courseId;
    const userId = req.params.userId;
    const course = await Course.findById(courseId)
        .populate('students').populate('teachers');
    if (course?.students.some(sid => sid === userId)){
        return res.send(UserRole.STUDENT);
    }
    if (course?.teachers.some(sid => sid === userId)){
        return res.send(UserRole.TEACHER);
    }
    return res.send('');
});

courseRouter.get('/:courseId/users', async (req:any, res:any) => {
    const courseId = req.params.courseId;
    const course = await Course.findById(courseId)
        .populate('students').populate('teachers').exec();
    const users = mergeCourseUsers(course);
    return res.status(200).send(users);
});

courseRouter.delete('/:courseId/users', async (req:any, res:any) => {
    const userIdsRaw: string[] = req.body;
    const userIds: string[] = userIdsRaw.filter(id => mongoose.isObjectIdOrHexString(id));
    
    const courseId = req.params.courseId;
    const course = await Course.findById(courseId);
    
    const remainTeachers = course?.teachers
        .filter((u) => !userIds.includes(u.toString()));
    const remainStudents = course?.students
        .filter((u) => !userIds.includes(u.toString()));

    console.log('PULL-STUDENTS', remainStudents);
    await Course.updateOne({ _id: courseId }, {
        teachers: remainTeachers,
        students: remainStudents,
    });

    // const result = await Course.findById(courseId)
    //     .populate('teachers').populate('students');

    await User.updateMany({ _id: {$in: userIds}}, { $pull: {courses: courseId}});
    // const users = await User.find({_id: {$in: userIds}});
    
    return res.status(200).send(userIds);
});

courseRouter.post('/:courseId/uploadAttachment', 
    async (req:any, res:any) => {
        const courseId = req.params.courseId;
        console.log('body', req.body);
        console.log('files', req.files);
        
        if(!req.files) {
            return res.send({
                success: false,
                message: 'No file uploaded'
            });
        }
        let attachmentFile = req.files.attachment;
        const attachment = await Attachment.create({
            fileName: attachmentFile.name,
            course: courseId
        });
        await Course.updateOne({_id: courseId}, { $push: {attachments: attachment}});
        const path = `./FileStore/Courses/${courseId}`;
        mkdirSync(path, {recursive: true});
        if (existsSync(path)){
            attachmentFile.mv(`${path}/${attachment.fileName}`);
            return res.json({
                fileName: attachment.fileName,
                createdAt: attachment.createdAt,
                content: '',
                _id: attachment._id,
            });
        }
    }
);

courseRouter.get('/:courseId/attachments',
    async (req:any, res:any) => {
        const course = await Course.findById(req.params.courseId);
        const attachments = Array.from(course?.attachments ?? []);

        res.json(attachments);
    }
);

courseRouter.delete('/:courseId/attachments/:attachmentId',
    async (req, res) => {
        const attachmentId = req.params.attachmentId
        await Course.updateOne({_id: req.params.courseId}, 
            {$pull: {attachments: {_id: attachmentId}}});
        await Attachment.deleteOne({_id: attachmentId});
        
        res.json(attachmentId);
    }
);

courseRouter.get('/:courseId/attachments/:attachmentId/download',
    async (req:any, res:any) => {
        const attachmentId = req.params.attachmentId
        const attachment = await Attachment.findOne({_id: attachmentId});
        const filepath = `./FileStore/Courses/${attachment?.course}/${attachment?.fileName}`;
        if (existsSync(filepath)){
            // return res.sendFile(filepath);
            return res.download(filepath);
        }else {
            return res.status(404).send('A fájl nem található!');
        }
    }
);

export default courseRouter;