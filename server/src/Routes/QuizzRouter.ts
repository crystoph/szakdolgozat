import express from "express";
import Quizz from "../Models/Schemas/Quizz";
import { quizzIdValidator, quizzDataValidator, quizzPublicValidator } from "../Models/Validators/QuizzValidator";
import validate from "../Middlewares/ValidationMiddleware";
import { AnswerType, AnswerTypes, QuizStatus, UserAnswerStatus, UserType } from "../Models/Schemas/enums/Enums";
import Course from "../Models/Schemas/Course";
import UserAnswer from "../Models/Schemas/UserAnswer";
import IUserAnswer from "../Models/Interfaces/IUserAnswer";
import mongoose from "mongoose";
import { dateToUtc, getAnswerReward, getDueDateFromNow, getTimeRemainingFromNow, makeDefaultAnswer, sumQuizReward } from "../Helpers/HelperFuntions";
import IQuizz from "../Models/Interfaces/IQuizz";
import { IAnswerView, IQuestionView, IQuizListView, IQuizzView, ISubmittedQuizView } from "../Models/ViewModels/IQuizzView";
import IAnswer from "../Models/Interfaces/IAnswer";
import ICourse from "../Models/Interfaces/ICourse";
import IUser from "../Models/Interfaces/IUser";
import User from "../Models/Schemas/User";

const quizzRouter = express.Router();

quizzRouter.post('/',
    validate(quizzDataValidator),
    async (req:any, res:any) => {
        const questions = req.body.questions;
        const userId = req.decodedToken._id;
        const canSolveFrom = req.body.canSolveFrom;
        const canSolveTo = req.body.canSolveTo;
        const solveTimeSeconds = req.body.solveTimeSeconds;
        for(var question of questions){
            question.canSystemCalc = AnswerTypes.find(at => at.type === question.type)?.canSystemCalc;
        }
        // console.log('questions', questions);
        questions.forEach((q:any) => delete q._id);

        const quizz = await Quizz.create({
            name: req.body.name,
            questions: questions,
            course: req.body.courseId,
            isPublic: false,
            createdBy: userId,
            canSolveFrom : canSolveFrom ?? null,
            canSolveTo: canSolveTo ?? null,
            secondsToSolve: solveTimeSeconds,
            status: QuizStatus.CREATED,
        });
        await Course.updateOne({ _id: quizz.course }, { $push: { quizzes: quizz } }).exec();
        res.json(quizz);
    }
);

quizzRouter.put('/:quizzId',
    validate([...quizzIdValidator, ...quizzDataValidator]),
    async (req:any, res:any) => {
        const quizId = req.params.quizzId;
        const userAnswer = await UserAnswer.find<IUserAnswer>({quizId});
        if (userAnswer && userAnswer.length > 0){
            return res.status(400).send('Nem módosíthat olyan kvízt amit már elkezdtek!');
        }
        const questions = req.body.questions;
        const from = dateToUtc(req.body.canSolveFrom);
        const to = dateToUtc(req.body.canSolveTo);
        let secondsToSolve : number = req.body.solveTimeSeconds;
        console.log('seconds', secondsToSolve);
        const userId = req.decodedToken._id;
        for(var question of questions){
            question.canSystemCalc = AnswerTypes.find(at => at.type === question.type)?.canSystemCalc;
            delete question._id;
        }
        // let secondsToSolve : number | null = null;
        // if (minutesToSolve){
        //     secondsToSolve = minutesToSolve * 60;
        // }
        await Quizz.updateOne({ _id: req.params.quizzId }, {
            name: req.body.name,
            questions: questions,
            canSolveFrom: from,
            canSolveTo: to,
            createdBy: userId,
            secondsToSolve,
        });
        const quizz = await Quizz.findOne({ _id: req.params.quizzId });
        res.json(quizz);
    }
);

quizzRouter.get('/:quizzId/submitted',
    validate([...quizzIdValidator]),
    async (req:any, res:any) => {
        const quizzId = req.params.quizzId;
        const thisQuiz = await Quizz.findOne<IQuizz>({_id: quizzId});
        const userAnswers = await UserAnswer.find({
            $and: [{quizId: quizzId}]
        }).populate('userId', 'name _id');
        const results:ISubmittedQuizView[] = [];
        for(const ua of userAnswers){
            if (!thisQuiz){
                continue;
            }
            const status = thisQuiz.status > ua.status ? thisQuiz.status : ua.status;
            const quiz: IQuizListView = {
                _id: thisQuiz._id,
                name: thisQuiz.name,
                canSolveFrom: thisQuiz.canSolveFrom ?? null,
                canSolveTo: thisQuiz.canSolveTo ?? null,
                isPublic: thisQuiz.isPublic,
                reward : sumQuizReward(thisQuiz.questions),
                minutesToSolve: thisQuiz.secondsToSolve ? Math.floor(thisQuiz.secondsToSolve / 60) : null,
                status: status,
                allReward: sumQuizReward(ua.questions),
            };

            const res: ISubmittedQuizView = {
                user: {
                    _id: ua.userId._id,
                    name: ua.userId.name,
                },
                quiz,
                userAnswerId: ua._id,
            }
            results.push(res);
        } 
        return res.json(results);
    }
);

quizzRouter.get('/', async (req:any, res:any) => {
    const courseId = req.query.courseId;
    const user = req.decodedToken;
    // const allQuizz = await Quizz.find<IQuizz>({course: courseId});
    let quizzes:IQuizz[] = [];
    const course = await Course.findById(courseId);
    const isTeacher = course?.teachers.some(tid => tid.toString() === user._id);
    // console.log('ISTEACHER', isTeacher);
    if (isTeacher || user.userType === UserType.ADMIN){
        quizzes = await Quizz.find<IQuizz>({course: courseId})
            .sort({canSolveFrom: 'asc'});
    }else {
        quizzes = await Quizz.find<IQuizz>({
            course: courseId,
            isPublic: true,
        })
        .sort({canSolveFrom: 'asc'});
    }
    const forUserId = req.decodedToken._id;
    let result: IQuizListView[] = [];
    for(const q of quizzes){
        const userAnswer = await UserAnswer.findOne<IUserAnswer>({ $and: [{quizId: q._id}, {userId: forUserId}]});
        const status = !userAnswer || q.status > userAnswer.status ? q.status : userAnswer.status;
        const res: IQuizListView = {
            _id: q._id,
            name: q.name,
            canSolveFrom: q.canSolveFrom ?? null,
            canSolveTo: q.canSolveTo ?? null,
            isPublic: q.isPublic,
            reward : sumQuizReward(q.questions),
            minutesToSolve: q.secondsToSolve ? Math.floor(q.secondsToSolve / 60) : null,
            status: status,
            allReward: userAnswer ? sumQuizReward(userAnswer.questions) : 0,
        };
        result.push(res);
    }
    
    res.json(result);
});

quizzRouter.delete('/:quizzId', async (req:any, res:any) => {
    const courseId = req.query.courseId;
    const quizId = req.params.quizzId;
    console.log(quizId, courseId);
    await Course.updateOne({ _id: courseId }, { $pull: { quizzes: {_id: quizId} } });
    await Quizz.deleteOne({ _id: quizId });
    res.json(quizId);
})

quizzRouter.post('/:quizzId/public',
    validate(quizzPublicValidator),
    async (req:any, res:any) => {
        const quizzId = req.params.quizzId;
        const value = req.body.isPublic;
        const userAnswer = await UserAnswer.find<IUserAnswer>({quizId: quizzId});
        if (userAnswer && userAnswer.length > 0){
            return res.status(400).send('Nem rejthet el olyan kvízt amit már elkezdtek!');
        }
        const quizStatus = value ? QuizStatus.PUBLISHED : QuizStatus.CREATED;
        const quizzBefore = await Quizz.findById(quizzId);
        if (quizzBefore && quizzBefore.status === quizStatus){
            return res.status(400)
                .send(`A kvíz státusza már ${quizStatus === QuizStatus.PUBLISHED ? 'publikált' : 'elrejtett'}!`);
        }
        await Quizz.updateOne<IQuizz>({_id: quizzId},
            {
                isPublic: value,
                status: quizStatus
            });
        const quizz = await Quizz.findById(quizzId);
        return res.json(quizz);
    }
);

quizzRouter.put('/:userAnswerId/close',
    async (req:any, res:any) => {
        const userAnswerId = req.params.userAnswerId;
        const userAnswer = await UserAnswer.findOne<IUserAnswer>({_id: userAnswerId});
        console.log('userAswer', userAnswer);
        if (!userAnswer){
            return res.status(404);
        }
        let questions:IQuestionView[] = req.body.questions;
        questions = questions.filter(q => q.type === AnswerType.ESSAY);
        // console.log('ESSAYS', questions);
        const updateQuestions = userAnswer?.questions.map(answerQ => {
            const commandQ = questions.find(q => q._id.toString() === answerQ._id.toString());
            if (!commandQ){
                return answerQ;
            }
            // console.log('commandQ', commandQ);
            answerQ.reward = commandQ.earnedReward;
            return answerQ;
        });
        // console.log('up', updateQuestions);
        const result = await UserAnswer.updateOne<IUserAnswer>({_id: userAnswerId},
        {
            questions: updateQuestions,
            status: UserAnswerStatus.GRADED,
        });
        // console.log('result', result);
        const allUserAnswer = await UserAnswer.find({quizId: userAnswer.quizId});
            if (allUserAnswer.every(qa => qa.status === UserAnswerStatus.GRADED && qa._id !== userAnswer._id)){
                await Quizz.updateOne<IQuizz>({_id: userAnswer.quizId}, { status: QuizStatus.CLOSED });
            }
        return res.json({success: true});
    }
);

const mergeQuizAnswer = async (userAnswer: IUserAnswer | null): Promise<IQuizzView | null> => {
    // console.log('userAnswera:', userAnswer);
    if (!userAnswer){
        return null;
    }
    const quiz = await Quizz.findOne<IQuizz>({_id: userAnswer.quizId});
    if (!quiz){
        return null;
    }
    const questionViews: IQuestionView[] = userAnswer.questions.map(q => {
        const answers: IAnswerView[] = q.answers.map(a => {
            const res: IAnswerView = {
                // userAnswerId: userAnswer._id,
                text: a.text,
                ordinal: a.ordinal,
                selected: a.selected,
            }
            // console.log('res', res);
            return res;
        }) ?? [];
        const originalQ = quiz.questions.find(qq => qq._id.toString() === q._id.toString());
        const res:IQuestionView = {
            answers: answers,
            text: q?.text,
            earnedReward: q.reward,
            ordinal: q.ordinal,
            type: q.type,
            reward: originalQ?.reward ?? 0,
            _id: q._id,
        };
        return res;
    });
    const status = quiz.status > userAnswer.status ? quiz.status : userAnswer.status;
    const quizView: IQuizzView = {
        name: quiz.name ?? null,
        canSolveFrom: quiz.canSolveFrom ?? null,
        canSolveTo: quiz.canSolveTo ?? null,
        _id: quiz._id,
        courseId: quiz.course,
        isPublic: quiz.isPublic,
        isTeacher: await isTeacherOfCourse(userAnswer.userId._id, quiz.course),
        questions: questionViews,
        userAnswerId: userAnswer._id,
        allReward: sumQuizReward(userAnswer.questions),
        reward: sumQuizReward(quiz.questions),
        isCompleted: userAnswer.questions.every(q => q.reward !== undefined),
        isSubmitted: userAnswer.isSubmitted,
        startTime: userAnswer.startTime,
        isLate: false,
        timeRemaining: getTimeRemainingFromNow(userAnswer.submitDateTime),
        status,
        userName: userAnswer.userId.name,
        solveTimeSeconds: userAnswer.status >= UserAnswerStatus.SUBMITTED && userAnswer.submitDateTime 
            ? Math.floor((userAnswer.submitDateTime?.getTime() - userAnswer.startTime.getTime()) / 1000) : 0
    }

    return quizView;
}

const startQuiz = async (quizz: IQuizz | null, userId: mongoose.ObjectId) => {
    if (!quizz){
        return null;
    }
    const userAnswer = {
        quizId: quizz._id,
        userId: userId,
        allReward: undefined,
        questions: quizz?.questions.map(q => makeDefaultAnswer(q)),
        isSubmitted: false,
        startTime: Date.now(),
        submitDateTime: getDueDateFromNow(quizz.secondsToSolve),
        status: UserAnswerStatus.STARTED,
    };

    const ua = await (await UserAnswer.create(userAnswer)).populate('userId', 'name _id');
    return await mergeQuizAnswer(ua);
}

quizzRouter.post('/:quizId/start',
    // validate(quizzPublicValidator),
    async (req:any, res:any) => {
        const now = new Date();
        const forUserId = req.decodedToken._id;
        const quizId = req.params.quizId;
        const quizz = await Quizz.findOne<IQuizz>({ _id: quizId});
        if (!quizz){
            return res.sendStatus(404);
        }
        const userAnswer = await UserAnswer.findOne<IUserAnswer>({ $and: [{quizId: quizId}, {userId: forUserId}]})
            .populate('userId', 'name _id');
        let isSubmitted = false;
        if (userAnswer){
            isSubmitted = userAnswer.status >= UserAnswerStatus.SUBMITTED;
        } 
        if (!quizz.isPublic && !isSubmitted){
            return res.status(401).send('A kvíz nem publikus!');
        }
        if ((quizz.canSolveFrom !== null && quizz.canSolveFrom > now)
            && !isSubmitted){
            return res.status(400).send('A kvíz kitöltése még nem engedélyezett!');
        }
        if ((quizz.canSolveTo !== null && quizz.canSolveTo < now)
            && !isSubmitted){
            return res.status(400).send('A kvíz kitöltése már nem engedélyezett!');
        }
        // console.log('forUserId', forUserId);
        if (!userAnswer){
            // console.log('USER ANSWER not EXISTS')
            const newStart = await startQuiz(quizz, forUserId);
            // console.log('newStart', newStart);
            return res.json(newStart);
        }
        const result = await mergeQuizAnswer(userAnswer);
        res.json(result);
    }
);


quizzRouter.get('/:quizId/user/:userId',
    // validate(quizzPublicValidator),
    async (req:any, res:any) => {
        const userId = req.params.userId;
        const quizId = req.params.quizId;
        const userAnswer = await UserAnswer.findOne<IUserAnswer>({ $and: [{quizId: quizId}, {userId: userId}]})
            .populate('userId', 'name _id');
        if (!userAnswer){
            return res.sendStatus(404);
        }
        const quizz = await Quizz.findOne<IQuizz>({ _id: userAnswer?.quizId});
        if (!quizz){
            return res.sendStatus(404);
        }
        const result = await mergeQuizAnswer(userAnswer);
        res.json(result);
    }
);

quizzRouter.post('/:quizzId/submit',
    // validate(quizzPublicValidator),
    async (req:any, res:any) => {
        const now = new Date();
        const userId = req.decodedToken._id;
        const quizId = req.params.quizzId;
        const commandQuiz:IQuizzView = req.body;
        const userAnswer = await UserAnswer.findOne<IUserAnswer>({_id: commandQuiz.userAnswerId, userId: userId})
            .populate('userId', 'name _id');
        if (!userAnswer){
            return res.SendStatus(400);
        }
        const quiz = await Quizz.findOne({_id: quizId});
        if (!quiz){
            return res.status(404);
        }
        const remainingTime = getTimeRemainingFromNow(userAnswer.submitDateTime)
        if (remainingTime && remainingTime < 0){
            console.log('No time remained', remainingTime);
            return res.status(400).send('A beadási határidő lejárt!');
        }

        userAnswer?.questions.forEach(q => {
            const quest = commandQuiz.questions.find(cq => cq._id.toString() === q._id.toString());
            const questionWithCorrectnswer = quiz?.questions.find(corrq => corrq._id.toString() === q._id.toString());
            if (!quest || !questionWithCorrectnswer){
                console.error('No Answer given');
            } else {
                let rew: number | null = getAnswerReward(questionWithCorrectnswer, quest.answers);
                const resAns: IAnswer[] | undefined = quest?.answers.map(a => {
                    return {selected: a.selected, ordinal: a.ordinal, text: a.text};
                });
                if (resAns){
                    q.reward = rew;
                    q.answers = resAns;
                }
            }
        });
        let quizStatus = UserAnswerStatus.SUBMITTED;
        if (userAnswer.questions.every(q => q.reward !== null)){
            quizStatus = UserAnswerStatus.GRADED;
            const allUserAnswer = await UserAnswer.find({quizId: quizId});
            if (allUserAnswer.every(qa => qa.status === UserAnswerStatus.GRADED && qa._id.toString() !== userAnswer._id.toString())){
                await Quizz.updateOne<IQuizz>({_id: quizId}, { status: QuizStatus.CLOSED });
            }
        }
        await UserAnswer.updateOne<IUserAnswer>({_id: userAnswer?._id}, { 
            questions: userAnswer?.questions, 
            status: quizStatus,
            submitDateTime: now,
        });
        const result = await mergeQuizAnswer(userAnswer);
        res.json(result);
    }
);

const isTeacherOfCourse = async (userId: mongoose.Schema.Types.ObjectId, courseId: mongoose.Schema.Types.ObjectId) => {
    const course = await Course.findOne<ICourse>({_id: courseId})
        .populate('teachers');
    const user = await User.findOne<IUser>({ _id: userId });
    const isTeacherOrAdmin = user?.userType === UserType.ADMIN
        || course?.teachers.some(t => t === userId) || false;
    // console.log('courseTeachers', courseId, course?.teachers);
    // console.log('userType', userId, user?.userType);
    return isTeacherOrAdmin;
}

quizzRouter.get('/:quizzId', //GETQ
    validate(quizzIdValidator),
    async (req:any, res:any) => {
        const forUserId = req.decodedToken._id;
        const quizId = req.params.quizzId;
        console.log(quizId)
        const quizz = await Quizz.findOne<IQuizz>({ _id: quizId });
        if (!quizz){
            return res.sendStatus(404);
        }
        const isTeacherOrAdmin = await isTeacherOfCourse(forUserId, quizz.course);
        const userAnswer = await UserAnswer.findOne<IUserAnswer>({user: forUserId, quiz: quizId})
            .populate('userId', 'name _id');
        if (!isTeacherOfCourse){
            return await mergeQuizAnswer(userAnswer);
        }

        const questionViews: IQuestionView[] = quizz.questions?.map(q => {
            const ans: any[] = q.answers.map(a => {
                const res = {
                    userAnswerId: '',
                    text: a.text,
                    ordinal: a.ordinal,
                    selected: a.selected,
                }
                return res;
            }) ?? [];
            const res:IQuestionView = {
                answers: ans,
                text: q?.text,
                earnedReward: null,
                ordinal: q.ordinal,
                type: q.type,
                reward: q.reward ?? 0,
                _id: q._id,
            };
            return res;
        });
        const result:IQuizzView = {
            name: quizz.name ?? null,
            canSolveFrom: quizz.canSolveFrom ?? null,
            canSolveTo: quizz.canSolveTo ?? null,
            _id: quizz._id,
            courseId: quizz.course,
            allReward: 0,
            isLate: false,
            isPublic: quizz.isPublic,
            isCompleted: false,
            questions: questionViews,
            isTeacher: isTeacherOrAdmin,
            userAnswerId: null,
            isSubmitted: null,
            reward: sumQuizReward(quizz.questions),
            timeRemaining : null,
            startTime: null,
            status: quizz.status,
            userName: null,
            solveTimeSeconds: quizz.secondsToSolve,
        };
        return res.json(result);
});


export default quizzRouter;