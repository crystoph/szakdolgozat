import request from 'supertest';
import app from '../index';

describe('api listens', () => {
    test('should get 200', async () => {
        const response = await request(app).get('/courses');

        expect(response.status).toBe(200);
    })
})