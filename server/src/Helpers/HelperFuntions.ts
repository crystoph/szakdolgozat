import { load } from "ts-dotenv";
import IAnswer from "../Models/Interfaces/IAnswer";
import IQuestion from "../Models/Interfaces/IQuestion";
import { AnswerType } from "../Models/Schemas/enums/Enums";

const env = load({
    QUIZ_THRESHHOLD_SECONDS: Number, 
 });


const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
const charactersLength = characters.length;
export function makeRandomStr(length: number) {
    let result = '';
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
}

export function getAnswerReward(question: IQuestion, answer: IAnswer[])
    : number | null
{
    if (!question.canSystemCalc || question.type === AnswerType.ESSAY){
        console.log(`${question.type}: noclac or ESSAY`);
        return null;
    }
    if(question.answers.length !== answer.length){
        console.log(`${question.type}: Answer length not the same`);
        return 0;
    }
    let result = question.reward; 
    let cans = question.answers;
    if (question.type === AnswerType.SORT){
        for(let i = 0; i<cans.length; i++){
            if (answer[i].ordinal !== cans[i].ordinal){
                result = 0;
            }
        }
        if (!result){
            console.log(`${question.type}: Answer ordinal incorrect`);
        }
        return result;
    }
    cans = question.answers.sort(ca => ca.ordinal);
    const ans = answer.sort(a => a.ordinal);
    // console.log('CORRECT ANS', cans)
    // console.log('ANSWERS', ans)
    // console.log('@@@@@@@@@@@@@');
    if (question.type === AnswerType.CONCRETE){
        for(let i = 0; i<cans.length; i++){
            if (cans[i].text !== ans[i].text){
                result = 0;
                console.log(`${question.type}: not equals ${i}: ${cans[i].text}, ${ans[i].text}`);
            }
        }
        return result;
    }
    if (question.type === AnswerType.PICKER){
        for(let i = 0; i<cans.length; i++){
            if (cans[i].selected !== ans[i].selected){
                result = 0;
                console.log(`${question.type}: Selected not equals ${i}`);
            }
        }
        return result;
    }
    console.error('ERROR, unknown question type');
    return null;
}

export function sumQuizReward(questions: IQuestion[]) : number {
    const result = questions.reduce((accumulator, currentValue) => {
        if (currentValue.reward !== null)
            return accumulator + currentValue.reward;
        return accumulator;
      },0);
    
      return result;
}

export function getTimeRemainingFromNow(due: Date | null) : number | null {
    if (!due){
        return null;
    }
    const ms = due.getTime() - new Date().getTime();
    return Math.floor(ms / 1000);
}

export function getDueDateFromNow(seconds: number | null) : Date | null {
    if (!seconds){
        return null;
    }
    const nowTime = new Date().getTime();
    let time = nowTime + seconds*1000 + env.QUIZ_THRESHHOLD_SECONDS;
    const result = new Date(time);
    return result;
}

function shuffle(array: IAnswer[]) {
    let currentIndex = array.length,  randomIndex;
  
    while (currentIndex != 0) {
  
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
  }

export function makeDefaultAnswer(question: IQuestion): IQuestion {
    if (question.type === AnswerType.ESSAY || question.type === AnswerType.CONCRETE){
        question.answers.forEach(a => a.text = '');
    }
    if (question.type === AnswerType.SORT){
        const sortAnswers = shuffle(question.answers);
        let i = 1;
        sortAnswers.forEach(sa => {
            sa.ordinal = i;
            i++;
        });
        question.answers = sortAnswers;
    }
    if (question.type === AnswerType.PICKER){
        question.answers.forEach(a => a.selected = false);
    }
    question.reward = null;
    
    return question;
}

export function dateToUtc(raw: string | undefined): Date | undefined {
    if (!raw){
        return undefined;
    }
    const value = Date.parse(raw);
    const date = new Date(value);
    console.log('date', date);
    if(date instanceof Date && !isNaN(date.valueOf())){
        // let result = new Date(0);
        // const inUtc = Date.UTC(date.getFullYear(), date.getMonth(), date.getDay(),
        // date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
        // result.setUTCSeconds(inUtc);
        // return result;
        return date;
    }
    return undefined;
}