import jsonwebtoken from "jsonwebtoken";
import mongoose from "mongoose";
import { load } from 'ts-dotenv';
import User from '../Models/Schemas/User';
import { Constants } from "./Constants";
const { TokenExpiredError } = jsonwebtoken;

const env = load({
    JWT_SECRET_TOKEN: String,
    JWT_REFRESH_TOKEN: String,
    ACCESS_TOKEN_EXPIRE: Number,
    REFRESH_TOKEN_EXPRIE: Number,
});

export async function generateAccessToken(id: mongoose.ObjectId): Promise<string> {
    const user = await User.findOne({_id: id}).exec();
    const encodedUser = {
        name: user?.name,
        _id: user?._id,
        userType: user?.userType
    };
    return jsonwebtoken.sign(encodedUser, env.JWT_SECRET_TOKEN, { expiresIn: `${env.ACCESS_TOKEN_EXPIRE.toString()}m` });
}

export async function generateRefreshToken(id: mongoose.ObjectId): Promise<string> {
    const user = await User.findOne({_id: id}).exec();
    const encodedUser = {
        _id: user?._id
    };
    return jsonwebtoken.sign(encodedUser, env.JWT_REFRESH_TOKEN, {expiresIn: `${env.REFRESH_TOKEN_EXPRIE.toString()}s`});
}

const catchError = (err:any, res:any) => {
    console.log(err);
    if (err instanceof TokenExpiredError) {
        return res.status(401).json({error: 'TokenExpiredError'});
    }

    return res.sendStatus(403).send({ message: "Unauthorized!" });
}

const notLoggedInResponse = {success: false, message: 'Csak belépett felhasználók számára elérhető!'};
export async function decodeToken(req: any, res: any, next: any){
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    console.log({method: req.method, route: req.path, token: token ? true : false});
    
    if (Constants.AllowAnonymusApiRoutes.some((anonymus) => 
    anonymus.method === req.method && anonymus.route === req.path))
    {
        next();
        return;
    }
    if (!token){
        return res.json(notLoggedInResponse);
    } 
    jsonwebtoken.verify(token, env.JWT_SECRET_TOKEN, async (err: any, decodedToken: any) => {
        if (err) {
            return catchError(err, res);
        }
        req.decodedToken = decodedToken;
        const user = await User.findOne({_id: decodedToken._id});
        req.shcoolId = user?.school;
        req.userId = user?._id;
        next();
    });
}

export async function getUserIdFromToken(req: any) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    let userId = '';
    if (token){
        jsonwebtoken.verify(token, env.JWT_SECRET_TOKEN, async (err: any, decodedToken: any) => {
            if (err) {
                console.log('Token error!');
            }else{
                return userId = decodedToken._id;
            }
        });
    }
    return userId;
}