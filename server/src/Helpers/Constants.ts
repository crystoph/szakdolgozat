export const availableCourseRoles = ['Teacher', 'Student'];

export class Constants {
    public static DEFAULT_TOKEN : String = 'no_token';
    public static AllowAnonymusApiRoutes = [
        {method: 'GET', route: '/news'},
        {method: 'POST', route: '/schools'},
        {method: 'GET', route: '/schools'},
        {method: 'GET', route: '/'},
        {method: 'GET', route: '/routes'},
    ]
};

export const defaultUserLenghts = {
    name: 8,
    password: 8
}