import express from 'express';
import { load } from 'ts-dotenv';
import cors from 'cors';
import mongoose from 'mongoose'
import bcrypt from 'bcrypt';
import jsonwebtoken from 'jsonwebtoken';
import { generateAccessToken, generateRefreshToken } from './Helpers/Auth';

import User from './Models/Schemas/User';
import School from './Models/Schemas/School';
import UserToken from './Models/Schemas/UserToken';

const app = express();
const env = load({
    JWT_SECRET_TOKEN: String,
    JWT_REFRESH_TOKEN: String,
    AUTH_PORT: Number,
    DATABASE: String,
    REFRESH_TOKEN_EXPRIE: Number,
}); 
let refreshTokens = [];
//middlewares
app.use(cors());
app.use(express.json());

app.post('/login', async (req, res) => {
    if (!req.body.email){
        res.json({
            success: false,
            message: 'Az e-mail cím megadása kötelező!'
        });
        return;
    }
    if (!req.body.password){
        res.json({
            success: false,
            message: 'A jelszó megadása kötelező!'
        });
        return;
    }
    try {
        const loggingUser  = await User.findOne({email: req.body.email}).exec();
        if (loggingUser == null){
            res.json({
                success: false,
                message: 'Nem létezik ilyen felhasználó!'
            });
            return;
        }
        if (await bcrypt.compare(req.body.password, loggingUser.password)
            || (loggingUser.isDefaultPassword && loggingUser.password === req.body.password)){
            const accessToken = await generateAccessToken(loggingUser._id);
            const refreshToken = await generateRefreshToken(loggingUser._id);
            let expireAt = new Date();
            expireAt.setSeconds(expireAt.getSeconds() + env.REFRESH_TOKEN_EXPRIE);
            await UserToken.create({
                userId: loggingUser._id,
                refreshToken: refreshToken,
                refreshTokenExpirationAt: expireAt
            });
            refreshTokens.push(refreshToken);
            const school = await School.findById(loggingUser.school);
            res.json({
                success: true,
                message: 'Sikeres belépés.',
                accessToken,
                refreshToken,
                user: {
                    name: loggingUser.name,
                    schoolName: school?.name,
                    schoolId: school?._id,
                    schoolUrl: school?.pageUrl,
                    userType: loggingUser.userType,
                    profilePicture: loggingUser.profilePicture,
                },
            });
        }else{
            res.json({
                success: false,
                message: 'Nem megfelelő jelszó!!'
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Szerver hiba történt.');
    }
});

app.delete('/logout', async (req, res) => {
    return res.sendStatus(200);
});

app.post('/refreshToken', 
    async (req, res) => {
        const requestToken = req.body.refreshToken;
        if (requestToken == null) {
            return res.status(403).json({ message: "RefreshToken megadása kötelező!" });
        }
        try {
            let refreshToken = await UserToken.findOne( { refreshToken: requestToken });
            console.log('Access token refresh required');
            if (!refreshToken) {
              res.status(403).json({ message: "Nem létezik ilyen RefreshToken!" });
              return;
            }
            if (refreshToken.refreshTokenExpirationAt.getTime() < new Date().getTime()){
                await UserToken.deleteOne({ _id: refreshToken._id });
                return res.status(403).json({message: "Jelentkezzen be újra!"});
            }
            const userId = refreshToken.userId;
            let newAccessToken = await generateAccessToken(userId);
            
            console.log("New access token: ", newAccessToken);
            return res.status(200).json({
                accessToken: newAccessToken,
                // refreshToken: refreshToken.refreshToken,
            });
        }catch(err){
            console.log(err);
            return res.status(500).send({ message: 'Szerver hiba!' });
        }
    }
);

app.listen(env.AUTH_PORT, async () => {
    await mongoose.connect(env.DATABASE)
        .catch(err => console.log('(AuthServer) No connection with db!', err))
        .then(() => console.log('(AuthServer) DB connected!'));

    console.log('AuthServer active');
});