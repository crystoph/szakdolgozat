import mongoose from "mongoose";
import ISchool from "../Interfaces/ISchool";
import Course from "./Course";
import News from "./News";
import User from "./User";

const SchoolSchema = new mongoose.Schema<ISchool>({
    name: String,
    pageUrl: String,
    emblem: String,
    users:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    news: [News.schema],
    courses: [Course.schema]
});

export default mongoose.model('School', SchoolSchema);