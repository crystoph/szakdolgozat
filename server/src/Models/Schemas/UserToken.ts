import mongoose from "mongoose";

const TokenSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    refreshToken: String,
    refreshTokenExpirationAt: Date,
});

const TokenModel = mongoose.model('Token', TokenSchema);

export default TokenModel;