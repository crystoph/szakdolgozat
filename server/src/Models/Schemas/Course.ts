import mongoose from "mongoose";
import ICourse from "../Interfaces/ICourse";
import Attachment from "./Attachment";
import News from "./News";
import Quizz from "./Quizz";

const CourseSchema = new mongoose.Schema<ICourse>({
    name: String,
    description: String,
    // announcements: [News.schema],
    school:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'School'
    },
    students: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    teachers: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    quizzes: [Quizz.schema],
    attachments: [Attachment.schema],
    createdAt: {
        type: Date,
        value: () => Date.now(),
        immutable: true,
    }
});

export default mongoose.model('Course', CourseSchema);