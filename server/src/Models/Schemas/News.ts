import mongoose from "mongoose";
import User from "./User";

const NewsSchema = new mongoose.Schema({
    title: String,
    description: String,
    authorName: String,
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    createdAt: {
        type: Date,
        value: () => Date.now(),
        immutable: true,
    },
    isPublic: {
        type: Boolean,
        default: false,
    },
    publishedAt: Date,
    updatedAt: {
        type: Date,
        value: () => Date.now(),
    },
    school:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'School'
    }
});

export default mongoose.model('News', NewsSchema);