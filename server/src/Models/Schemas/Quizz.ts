import mongoose from "mongoose";
import IQuizz from "../Interfaces/IQuizz";
import Course from "./Course";
import Question from "./Question";
import User from "./User";
import { QuizStatus } from "./enums/Enums";

const QuizzSchema = new mongoose.Schema<IQuizz>({
    name:String,
    questions: [Question.schema],
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    isPublic: Boolean,
    course: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Course',
    },
    canSolveFrom: {
        type: Date || undefined,
        default: undefined
    },
    canSolveTo: {
        type: Date || undefined,
        default: undefined
    },
    secondsToSolve: {
        type: Number,
        default: null,
    },
    status: {
        type: Number,
    }
});

export default mongoose.model('Quizz', QuizzSchema);