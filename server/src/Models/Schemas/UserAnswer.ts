import mongoose from "mongoose";
import IUserAnswer from "../Interfaces/IUserAnswer";
import Question from "./Question";

const UserAnswerSchema = new mongoose.Schema<IUserAnswer>({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    questions: [Question.schema],
    quizId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Quiz'
    },
    isSubmitted: {
        type: Boolean,
        default: null,
    },
    submitDateTime: {
        type: Date,
        default: null,
    },
    startTime: {
        type: Date,
        // value: () => Date.now(),
        // immutable: true,
    },
    status: {
        type: Number,
    }
});


export default mongoose.model('UserAnswer', UserAnswerSchema);
