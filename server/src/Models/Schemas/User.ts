import mongoose from "mongoose"
import { UserRole, UserType } from "./enums/Enums";
import { Constants } from "../../Helpers/Constants";
import IUser from "../Interfaces/IUser";

const userSchema = new mongoose.Schema<IUser>({
    name: String,
    userType: String,
    // positionType: String,
    
    school:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'School'
    },

    isDefaultPassword: {
        type: Boolean,
        default: false,
    },

    email: {
        type: String,
        lowercase: true,
        validate: {
            validator : isEmail,
            message: 'Email validácó sikertelen.'
        }
    },

    password: {
        type: String,
        required: true,
    },

    courses: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Course'
        }
    ],

    profilePicture: String,

    createdAt: {
        type: Date,
        value: () => Date.now(),
        immutable: true,
    },
    updatedAt: {
        type: Date,
        value: () => Date.now(),
    },
})

export default mongoose.model('User', userSchema)

//validators
function isEmail(email: string){
    return email.includes('@') &&
           email.includes('.')
}
