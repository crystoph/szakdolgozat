import mongoose from "mongoose";
import IAttachment from "../Interfaces/IAttachment";

const AttachmentSchema = new mongoose.Schema<IAttachment>({
    fileName: String,
    createdAt: {
        type: Date,
        default: () => Date.now(),
        // immutable: true,
    },
    course:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Course'
    }
});

export default mongoose.model('Attachment', AttachmentSchema);