import mongoose from "mongoose";
import IQuestion from "../Interfaces/IQuestion";
import Answer from './Answer'

const QuestionSchema = new mongoose.Schema<IQuestion>({
    text: String,
    type: String,
    ordinal: Number,
    reward: Number,
    answers: [Answer.schema],
    canSystemCalc: Boolean,
});

export default mongoose.model('Question', QuestionSchema);