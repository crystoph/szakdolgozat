import mongoose from "mongoose";
import IAnswer from "../Interfaces/IAnswer";
import { AnswerType } from "./enums/Enums";

const AnswerSchema = new mongoose.Schema<IAnswer>({
    text:String,
    ordinal: Number,
    selected: Boolean,
});

const AnswerModel = mongoose.model('Answer', AnswerSchema);

// AnswerModel.discriminator(AnswerType.PICKER, 
//     new mongoose.Schema({
//         pickOptions: [{
//             optionText: String,
//             isPicked: Boolean
//         }]
//     })
// );



export default AnswerModel;
