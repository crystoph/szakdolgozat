export enum UserType {
    ADMIN = 'ADMIN',
    USER = 'USER',
}

export enum UserRole {
    STUDENT = 'Student',
    TEACHER = 'Teacher'
}

export enum AnswerType {
    PICKER = 'Picker',
    ESSAY = 'Essay',
    SORT = 'Sort',
    CONCRETE = 'Concrete',
}

export const AnswerTypes = [
    {
        type: AnswerType.ESSAY,
        canSystemCalc: false
    },
    {
        type: AnswerType.SORT,
        canSystemCalc: true
    },
    {
        type: AnswerType.PICKER,
        canSystemCalc: true
    },
    {
        type: AnswerType.CONCRETE,
        canSystemCalc: true
    }
]

export enum QuizStatus {
    CREATED = 1,
    PUBLISHED = 2,
    CLOSED = 6,
}

export enum UserAnswerStatus {
    STARTED = 3,
    SUBMITTED = 4,
    GRADED = 5,
}