import mongoose from "mongoose";
import IAttachment from "./IAttachment";
import IQuizz from "./IQuizz";

export default interface ICourse {
    _id: mongoose.ObjectId,
    name: string,
    description: string,
    school: mongoose.ObjectId,
    students: mongoose.ObjectId[],
    teachers: mongoose.ObjectId[],
    quizzes: IQuizz[],
    attachments: IAttachment[],
    createdAt: Date,
}