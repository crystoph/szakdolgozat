export default interface IAnswer {
    text: string,
    ordinal: number,
    selected: boolean,
}