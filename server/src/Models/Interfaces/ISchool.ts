import ICourse from "./ICourse";
import INew from "./INew";
import IUser from "./IUser";

export default interface ISchool {
    name: string,
    pageUrl: string,
    emblem: string,
    users: IUser[],
    news: INew[],
    courses: ICourse[],
}