import mongoose from "mongoose";

export default interface IAttachment {
    fileName: string,
    createdAt: Date,
    course: mongoose.ObjectId, 
}