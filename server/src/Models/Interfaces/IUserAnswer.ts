import mongoose from "mongoose";
import IAnswer from "./IAnswer";
import IQuestion from "./IQuestion";
import { UserAnswerStatus } from "../Schemas/enums/Enums";
import IUser from "./IUser";

export default interface IUserAnswer {
    _id: mongoose.Schema.Types.ObjectId,
    userId: IUser,
    quizId: mongoose.Schema.Types.ObjectId,
    questions: IQuestion[],
    isSubmitted: boolean | null,
    submitDateTime: Date | null,
    startTime: Date,
    status: UserAnswerStatus,
}