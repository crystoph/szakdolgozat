import mongoose from "mongoose";
import { UserType } from "../Schemas/enums/Enums";

export default interface IUser {
    _id: mongoose.ObjectId,
    name: string,
    userType: UserType,
    school: mongoose.ObjectId,
    isDefaultPassword: boolean,
    email: string,
    password: string,
    courses: mongoose.ObjectId[],
    createdAt: Date,
    updatedAt: Date,
    profilePicture: string,
}