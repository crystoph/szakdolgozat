import mongoose from "mongoose";
import IQuestion from "./IQuestion";
import { QuizStatus } from "../Schemas/enums/Enums";

export default interface IQuizz {
    _id: mongoose.ObjectId,
    name: string,
    questions: IQuestion[],
    createdBy: mongoose.ObjectId,
    isPublic: boolean,
    course: mongoose.ObjectId,
    canSolveFrom: Date | null,
    canSolveTo: Date | null,
    secondsToSolve: number | null,
    status: QuizStatus,
}