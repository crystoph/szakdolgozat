import mongoose from "mongoose";
import { AnswerType } from "../Schemas/enums/Enums";
import IAnswer from "./IAnswer";

export default interface IQuestion {
    _id: mongoose.Schema.Types.ObjectId
    text: string,
    type: AnswerType,
    ordinal: number,
    reward: number | null,
    answers: IAnswer[],
    canSystemCalc: boolean,
}