import mongoose from "mongoose";

export interface  IUserListView {
    name: string,
    _id: mongoose.ObjectId,
}