import mongoose from "mongoose";
import { AnswerType, QuizStatus, UserAnswerStatus } from "../Schemas/enums/Enums";
import { IUserListView } from "./IUserListView";

export interface IQuizzView {
    name: string | null,
    canSolveFrom: Date | null,
    canSolveTo: Date | null,
    questions: IQuestionView[],
    _id: mongoose.ObjectId,
    courseId: mongoose.ObjectId,
    allReward: number | null,
    reward: number,
    isLate: boolean,
    isCompleted: boolean | null,
    isSubmitted: boolean | null,
    isPublic: boolean,
    isTeacher: boolean,
    userAnswerId: mongoose.ObjectId | null,
    timeRemaining: number | null,
    startTime: Date | null,
    status: QuizStatus | UserAnswerStatus,
    userName: string | null,
    solveTimeSeconds: number | null,
}

export interface IQuestionView {
    answers: IAnswerView[]
    text: string
    type: AnswerType,
    reward: number,
    earnedReward: number | null,
    ordinal: number,
    _id: mongoose.ObjectId,
}

export interface IAnswerView {
    text: string,
    ordinal: number,
    selected: boolean,
}

export interface IQuizListView {
    name: string | null,
    canSolveFrom: Date | null,
    canSolveTo: Date | null,
    _id: mongoose.ObjectId,
    isPublic: boolean,
    reward: number,
    minutesToSolve: number | null,
    status: QuizStatus | UserAnswerStatus,
    allReward: number | null,
}

export interface ISubmittedQuizView {
    user: IUserListView,
    quiz: IQuizListView,
    userAnswerId: mongoose.ObjectId,
}