import { body, param } from "express-validator";
import mongoose from "mongoose";
import School from "../Schemas/School";

export const schoolDataValidator = [
    body('schoolName').customSanitizer((s:String) => s.trim())
        .notEmpty().withMessage('Az iskola nevét kötelező megadni!')
        .custom(async (sn:String) => {
            const school = await School.findOne({name: sn});
            return school !== null;
        }).withMessage('Az iskola neve már foglalt!'),

    body('pageUrl')
        .customSanitizer((pUrl:String, {req}) => pUrl || req.body.schoolName)
        .notEmpty().withMessage('Az iskola url-címének megadása kötelező!')
        .custom(async (purl:String) => {
            const school = await School.findOne({pageUrl: purl});
            console.log('validalok', purl);
            return school !== null;
        }).withMessage('A megadott url cím már foglalt!'),
]

export const schoolIdValidator = [
    param('schoolId')
        .notEmpty().isMongoId()
        .custom((id:mongoose.ObjectId) => School.exists({_id: id})).withMessage('Nem létezik a megadott iskola!')
        .custom(async (id:mongoose.ObjectId, {req}) => {
            const school = await School.findById(id);
            return school?.users.some((u:any)=> u === req.userId);
        }).withMessage('Nincs joga!'),
]