import { body, param } from "express-validator";
import mongoose from "mongoose";
import IQuizz from "../Interfaces/IQuizz";
import IUser from "../Interfaces/IUser";
import Course from "../Schemas/Course";
import { UserType } from "../Schemas/enums/Enums";
import Quizz from "../Schemas/Quizz";
import User from "../Schemas/User";

export const quizzDataValidator = [
    body('name').notEmpty().escape().withMessage('A kvíz nevét kötelező megadni!'),
    // body('students').isArray().withMessage('A diákok megadása kötelező!')
    //     .custom(async (studs:any) => {
    //         return studs.every(async (t:any) => await User.exists({_id: t._id}));
    //     }).withMessage('Hibás felhasználót adott meg!')
    //     .custom(async (studs:any, {req}) => {
    //         const course = await Course.findOne({_id: req.param.courseId});
    //         console.log(course);
    //         return studs.every(async (t:any) => {
    //             course.students.includes(t._id);
    //         });
    //     }),
    body('courseId').notEmpty().withMessage('A kurzust kötelező megadni!'),
];

export const quizzIdValidator = [
    param('quizzId').custom(async _id => {
            return await Quizz.exists({_id});
        }).escape().withMessage('Nem létező kvíz.'),
]

export const quizzPublicValidator = [
    
]

export const isQuizzVisible = async (quiz: IQuizz, user: IUser):Promise<boolean> => {
    if (user?.userType === UserType.ADMIN){
        return true;
    }
    // const quiz = await Quizz.findById(quizId);
    const course = await Course.findById(quiz.course);
    if (course?.teachers.some((t:any) => t._id === user._id)){
        return true;
    }
    const now = new Date();
    if (course?.students.some((t:any) => t._id === user._id)){
        return quiz.isPublic && quiz.canSolveFrom !== null && quiz.canSolveFrom < now;
    }
    return false;
}