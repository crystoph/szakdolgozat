import { body, param } from "express-validator";
import mongoose from "mongoose";
import User from "../Schemas/User";

export const userPasswordValidator = [
    body('password')
        .notEmpty().withMessage('A jelszó megadása kötelező')
        .isLength({min: 8}).withMessage('A jelszónak legalább 8 karakter hosszúnak kell lennie!'),
]

export const userDataValidator = [
    body('email')
        .notEmpty().withMessage('Az e-mail сím megadása kötelező!')
        .isEmail().withMessage('Az e-mail cím formátuma helytelen')
        .custom(async (un:String) => (await User.findOne({email: un})))
            .withMessage('Az email cím már foglalt!'),

    body('userName')
        .notEmpty().withMessage('A felhasználónév megadása kötelező!')
        .custom(async (un:String) => !(await User.findOne({name: un}))).withMessage('A felhasznláónév foglalt'),
]

export const userIdValidator = [
    param('userId')
        .notEmpty().isMongoId()
        .custom((id:mongoose.ObjectId) => User.exists({_id: id})).withMessage('Nem létezik a megadott felhasználó!')
        .custom(async (id:mongoose.ObjectId, {req}) => {
            return req.userId === id;
        }).withMessage('Nincs joga!'),
]