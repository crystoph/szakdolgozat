import { body, param } from "express-validator";
import Course from "../Schemas/Course";

export const courseDataValidator = [
    body('name').notEmpty().escape().withMessage('A kurzus nevét kötelező megadni!'),
];

export const courseQuizzAssignValidator = [
    param('courseId').custom(async (id, {req}) => {
        const course = await Course.findOne({_id: id}).populate('quizzes');
        return course?.quizzes.every((q:any) => q._id !== req.params?.quizzId);
    }).withMessage('A kvízt már a kurzushoz rendelték!'),
]

export const courseIdValidator = [
    param('courseId').custom(async _id => {
            return await Course.exists({_id});
        }).escape().withMessage('Nem létező kurzus.'),
]