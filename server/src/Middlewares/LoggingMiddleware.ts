
export function logger(req:any, res:any, next:Function ): void {
    try{
        console.info(new Date().toLocaleTimeString() + '| ' + req.method.toUpperCase() + ': ' + req.url);
        next();
    }catch(ex){
        console.log('errorhandler mdw');
        next(ex);
    }
}

export async function errorHandler(err:any, req:any, res:any, next:any) {
    console.error(err);
    res.status(500).json({success: false, message: 'Szerver hiba'});
  };

export function outPutLogger(req:any, res:any, next:Function ): void {
    try{
        console.info('Server response', res);
        next();
    }catch(ex){
        console.error(ex);
        res.status(500).json({success: false, message: 'Szerver hiba'});
    }
}