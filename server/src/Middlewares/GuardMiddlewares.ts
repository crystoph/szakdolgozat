import mongoose from "mongoose";
import { UserType } from "../Models/Schemas/enums/Enums";
import School from "../Models/Schemas/School";
import User from "../Models/Schemas/User";
import Course from "../Models/Schemas/Course";

export function isAdmin(req:any, res:any, next:Function ): void {
    if(!req.decodedToken){
        console.log('nincs token');
        
        // res.success = false;
        // res.message = 'Erre csak Admin felhasználó jogosult!';
        return res.sendStatus(401);
    }
    if(!req.decodedToken.hasOwnProperty('name')){
        console.log('nincs név');
        
        res.success = false;
        res.message = 'Erre csak Admin felhasználó jogosult!';
        return;
    }

    User.findOne({_id: req.decodedToken._id}).exec().then(user => {
        if(!user){
            console.log('nincs user');
            
            res.success = false;
            res.message = 'Erre csak Admin felhasználó jogosult!';
            return;
        }
        if(user.userType === UserType.ADMIN)
            console.log('minden jót, Admin Úr!');
            next();
    });
}

export async function isAdminOrTeacherOfCourse(req:any, res:any, next:Function ) {
    if(!req.decodedToken || !req.decodedToken._id){
        console.log('No token');
        return res.sendStatus(401);
    }
    const user = await User.findOne({_id: req.decodedToken._id});
    if (!user){
        console.log('No token!');
        return res.sendStatus(401);
    }
    const rawCourseId = req.params.courseId ?? req.query.courseId;
    if (!rawCourseId || !mongoose.isValidObjectId(rawCourseId)){
        console.log('Bad courseId!');
        return res.sendStatus(400);
    }
    const course = await Course.findOne({_id: rawCourseId});
    if(!course){
        console.log('Course not exists!');
        return res.sendStatus(400);
    }
    if (course.school.toString() !== user.school.toString()){
        console.log('Course in other school!');
        return res.sendStatus(403);
    }
    if (user.userType === UserType.ADMIN
        || course.teachers.some(id => id.toString() === user._id.toString())){
        return next();
    }
    console.log('Cannot determine...');
    return res.sendStatus(400);
}

export async function isSchoolVisible(req:any, res:any, next:Function ): Promise<void> {
    const rawSchoolId = req.params.schoolId ?? req.query.schoolId;
    if (!rawSchoolId){
        console.log('schoolId missing!');
        res.sendStatus(400);
        return;
    }
    if (!mongoose.isValidObjectId(rawSchoolId)){
        console.log('schoolId not valid!');
        res.sendStatus(400);
        return;
    }
    const schoolId = new mongoose.Schema.Types.ObjectId(rawSchoolId);
    const userId:mongoose.ObjectId = req.decodedToken._id;
    const user = await User.findById(userId);
    const userSchoolId = user?.school;
    if(!userSchoolId || schoolId === userSchoolId){
        return res.sendStatus(403);
    }
    req.body.schoolId = schoolId;
    next();
}