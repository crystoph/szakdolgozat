export const environment = {
  production: true,
  allowedDomains: ['localhost:1000', 'localhost:1010'],
  appName: 'Private LMS',
};
