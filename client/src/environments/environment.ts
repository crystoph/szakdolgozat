export const environment = {
    production: false,
    allowedDomains: [
        'localhost:1000',
        'localhost:1010',
    ],
    appName: 'Private LMS',
};