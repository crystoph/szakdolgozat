export interface NoDataResponse {
    success: boolean,
    message: string
}