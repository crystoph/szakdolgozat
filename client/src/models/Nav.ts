export interface Nav {
    navigationLink: string
    name: string
    active: boolean
}