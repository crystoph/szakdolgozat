import { User, emptyUser } from "./User"

export interface News {
    title: string,
    _id: string,
    description: string,
    isPublic: boolean,
    publishedAt: Date | null,
    createdBy: NewCreator,
}

export interface NewCreator {
    id: string,
    name: string,
    profilePicture: string,
}

export const emptyUserSimpleData: NewCreator = {
    id: '',
    name: '',
    profilePicture: '',
}

export const emptyNews: News = {
    title: '',
    _id: '',
    description: '',
    isPublic: false,
    publishedAt: null,
    createdBy: emptyUserSimpleData,
}