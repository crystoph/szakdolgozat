export interface School {
    schoolName: string,
    _id: string,
    pageUrl: string,
    emblem: string,
}

export const emptySchool: School = {
    schoolName: '',
    _id: '',
    pageUrl: '',
    emblem:'',
}