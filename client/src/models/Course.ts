export interface Course {
    _id: string
    name: string
    description: string,
    teachers: string[],
    students: string[],
}


export interface Attachment {
    fileName: string,
    createdAt?: Date,
    panelOpen?: boolean,
    hover?: boolean,
    extension?: string ,
    content: string,
    file: File | null,
    _id: string,
}

// export const emptyAttachment: Attachment = {
//     fileName: '',
//     file: null,
// }

export const emptyCourse : Course = {
    name: '',
    _id: '',
    description: '',
    teachers: [],
    students: [],
}