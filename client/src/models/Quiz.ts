import { QuizSolverUser } from "./User"
import { AnswerType, QuizStatus } from "./enums/Enums"

export interface Answer {
    text: string
    selected: boolean | undefined
    correct: boolean | undefined
    hover: boolean,
    ordinal: number,
    // userAnswerId: string,
}

export interface Question {
    answers: Answer[]
    text: string
    type: AnswerType,
    reward: number,
    earnedReward: number | null,
    ordinal: number,
    _id: string,
    hover: boolean,
}

export function getEmptyAnswer(type: AnswerType): Answer[] {
    const answers: Answer[] = [];
    if(type === AnswerType.ESSAY || type === AnswerType.CONCRETE){
        answers.push({ text: '', selected: undefined, correct: undefined, hover: false, ordinal: 1});
    }
    if (type === AnswerType.PICKER){
        answers.push({ text: '', selected: false, correct: undefined, hover: false, ordinal: 1});
    }
    if(type === AnswerType.SORT){
        for (let i = 1; i < 5; i++){
            answers.push({ text: '', selected: undefined, correct: undefined, hover: false, ordinal: i});
        }
    }
    return answers;
}

export function getEmptyQuestion(ordinal: number, type: AnswerType): Question {
    const question: Question = {
        text: '',
        ordinal,
        hover:false,
        type,
        answers: [],
        reward: 1,
        earnedReward: null,
        _id: '',
    };
    question.answers = getEmptyAnswer(type);
    return question;
}

export interface Quiz {
    name: string | null,
    canSolveFrom: Date | null,
    canSolveTo: Date | null,
    questions: Question[],
    _id: string,
    courseId: string,
    allReward: number | null,
    reward: number,
    isLate: boolean,
    isPublic: boolean,
    isTeacher: boolean,
    userAnswerId: string | null,
    isCompleted: boolean | null,
    isSubmitted: boolean | null,
    timeRemaining: number | null,
    startTime: Date | null,
    status: QuizStatus,
    userName: string | null,
    solveTimeSeconds: number | null,
}

export const emptyQuiz: Quiz = {
    name: '',
    canSolveFrom: null,
    canSolveTo: null,
    questions: [],
    _id: 'tmp',
    courseId: '',
    allReward: null,
    reward: 0,
    isLate: false,
    isPublic: false,
    isTeacher: false,
    isCompleted: false,
    userAnswerId: null,
    isSubmitted: null,
    timeRemaining: null,
    startTime: null,
    status: QuizStatus.CREATED,
    userName: null,
    solveTimeSeconds: null,
}

export interface QuizListItem {
    name: string | null,
    canSolveFrom: Date | null,
    canSolveTo: Date | null,
    _id: string,
    isPublic: boolean,
    reward: number,
    minutesToSolve: number | null,
    status: QuizStatus,
    allReward: number | null,
}

export interface SubmittedQuizListItem {
    user: QuizSolverUser,
    quiz: QuizListItem,
    userAnswerId: string,
}