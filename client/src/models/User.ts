export interface User{
    email: string,
    name: string,
    password: string,
    _id: string,
    schoolName: string,
    schoolId: string,
    schoolUrl: string,
    positionType: string,
    isDefaultPassword: boolean,
    profilePicture: string
}

export const emptyUser: User = {
    email: '',
    name: '',
    password: '',
    _id: '',
    schoolName : '',
    schoolId: '',
    schoolUrl: '',
    positionType: '',
    isDefaultPassword: false,
    profilePicture: '',
}

export interface QuizSolverUser {
    name: string,
    _id: string,
}
export interface CourseUser {
    _id: string;
    name: string;
    selected: boolean;
    userRole: string,
}