export function internalErrorMessage(component:string, additionalMessage: string){
    console.error(component + ' : ' + additionalMessage);
}

export function internalWarningMessage(component:string, additionalMessage: string){
    console.warn(component + ' : ' + additionalMessage);
}

export function formatDate(d: Date) {
    return [d.getFullYear(), d.getMonth(), d.getDay()].join('-');
}

export function transformTimeSpan(value: number, withExplanation: boolean = false) : string {
    const hours: number = Math.floor(value / 3600);
    const minutes: number = Math.floor(value / 60);
    const seconds: number = value - minutes * 60;
    if (!withExplanation){
        return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    }
    return `${hours}ó ${minutes}p ${seconds}mp`;
}