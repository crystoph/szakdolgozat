export class AppRoutes {
    static main:string = 'fooldal';
    static register:string = 'regisztracio';
    static login:string = 'belepes';
    static users:string = 'felhasznalok';
    static courses:string = 'kurzusok';
    static school:string = 'iskola';
    static addCourse:string = AppRoutes.courses + '/hozzaadas';
    static quizzes:string = 'kvizek';
    static submitted:string = 'bekuldott';
    static schoola:string = 'iskola';
}

export class APIRoutes {
    private static host:string = 'http://localhost:1000';
    private static authHost:string = 'http://localhost:1010';
    static Login: string = `${this.authHost}/login`;
    static Logout: string = `${this.authHost}/logout`;
    static Users: string = `${this.host}/users`;
    static News: string = `${this.host}/news`;
    static Courses: string = `${this.host}/courses`;
    static Schools: string = `${this.host}/schools`;
    static Quizzes: string = `${this.host}/quizzes`;
    static RefreshToken: string = `${this.authHost}/refreshToken`;
}