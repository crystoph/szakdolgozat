import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { NavigationService } from '../services/navigation.service';
import { CourseService } from '../services/course.service';

@Injectable({
    providedIn: 'root'
})
export class TeacherGuard implements CanActivateChild {
    constructor(
        private authService: AuthService,
        private navigationService: NavigationService,
        private courseService: CourseService,
    ){}

    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        if(this.courseService.isTeacher || this.authService.isAdmin()){
            return true;
        }else{
            this.navigationService.deleteLast();
            return false;
        }
  }

}
