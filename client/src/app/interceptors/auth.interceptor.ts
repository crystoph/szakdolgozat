import { Inject, Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpErrorResponse,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private isRefreshing$ = new BehaviorSubject<boolean>(false);

    constructor(
        private authService: AuthService,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const idToken = this.authService.getAccessToken();
        console.log('Intercepting...token:');

        if (idToken) {
            const cloned = this.addTokenHeader(request, idToken);
            return next.handle(cloned).pipe(
                catchError(error => {        
                    if (error.status === 401) {
                        return this.handle401Error(request, next);
                    }
                    return throwError(error);
                })
              );
        }
        else {
            return next.handle(request);
        }
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing$.getValue()) {
            this.isRefreshing$.next(true);
      
            const refreshToken = this.authService.getRefreshToken();
      
            if (refreshToken) {
              return this.authService.refreshAccessToken(refreshToken).pipe(
                tap(({accessToken}) => {
                  this.isRefreshing$.next(false);
                  console.log('saving new: ', accessToken);
                  this.authService.saveAccesToken(accessToken);
                //   this.authService.saveRefreshToken(refreshToken);
                }),
                switchMap(({accessToken}) => {
                  return next.handle(this.addTokenHeader(request, accessToken));
                }),
                catchError((err) => {
                  this.isRefreshing$.next(false);
                //   this.authService.onError();
                  return throwError(err);
                })
              );
            }
      
            this.isRefreshing$.next(false);
            // this.authService.onError();
            return throwError('Missing refresh token');
          }

          return this.isRefreshing$.pipe(
            filter((is) => !is),
            take(1),
            switchMap(() => {
              const accessToken = this.authService.getAccessToken();
              return next.handle(this.addTokenHeader(request, accessToken??''));
            })
          );
    }

    private addTokenHeader(request: HttpRequest<any>, token: string) {
        return request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`,
          },
        });
    }
}
