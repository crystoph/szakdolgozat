import { compareSegments } from '@angular/compiler-cli/src/ngtsc/sourcemaps/src/segment_marker';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { UserService } from 'src/app/services/user.service';
import { emptyUser, User } from 'src/models/User';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
    user: User = emptyUser;
    passwordRepeat: string = '';
    isPasswordUpdate: boolean = false;
    profilePicture: string;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute,
        private authService: AuthService,
        private navigationService: NavigationService,
        private toastrService: ToastrService,
    ) { }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id') ?? '';
        this.userService.getUser(id).subscribe(user => {
            this.profilePicture = user.profilePicture;
            this.user = user;
        });
    }

    isLoggedInUser(): boolean {
        // console.log(this.authService.getDecodedToken()._id, this.user._id);
        return this.authService.getDecodedToken()._id === this.user._id;
    }

    goPrev(){
        this.navigationService.goPrevPage();
    }

    onSave(): void {
        if (this.isPasswordUpdate &&
            (this.user.password && (this.user.password !== this.passwordRepeat || this.user.password.length < 8))){
            this.toastrService.error('Hibás jelszó!');
            return;
        }
        if (this.isPasswordUpdate){
            this.userService.changePassword(this.user).subscribe(res => {
                this.isPasswordUpdate = false;
                this.user.password = '';
                this.passwordRepeat = '';
                if (res.success){
                    this.toastrService.success(res.message);
                }else{
                    this.toastrService.error(res.message);
                }
            });
        }
        this.userService.updateUser(this.user).subscribe(res => {
            this.user = res;
            this.toastrService.success('A felhasználó adatai módosultak!');
        }, err => {
            this.toastrService.error(err.error.errors[0].msg);
        });
    }

    onFileSelected(event:any){
        const file:File = event.target.files[0];
        if (file){
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                const base64 = reader.result as string;
                this.profilePicture = base64;
                console.log(base64);
            };
        }else{
            this.toastrService.error('Nem sikerült a fájl kiválasztása!');
        }
    }

    uploadFile(){
        this.user.profilePicture = this.profilePicture;
        if (this.user.profilePicture){
            this.userService.uploadProfile(this.user.profilePicture, this.user._id).subscribe(res => {
                this.toastrService.success('Sikeres profil hozzáadás!')
                this.authService.setUserProfilePicture(this.user.profilePicture);
            }, err => {
                this.toastrService.error('Nem sikerült a feltöltés!');
            });
        }
    }
}
