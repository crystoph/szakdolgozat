import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { formatDate } from 'src/app/helpers/HelperFunctions';
import { AuthService } from 'src/app/services/auth.service';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { NewService } from 'src/app/services/new.service';
import { News, emptyNews } from 'src/models/New';

@Component({
  selector: 'app-new-page',
  templateUrl: './new-page.component.html',
  styleUrls: ['./new-page.component.scss'],
})
export class NewPageComponent implements OnInit {
  new: News = emptyNews;
  isUpdate: boolean = false;

  constructor(
    private newService: NewService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService,
    private mainService: MainService,
    private navigationService: NavigationService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('newId');
    if (!id) {
      this.toastrService.error('Nem található az adott cikk!');
      return;
    }
    if (id !== 'tmp') {
      this.isUpdate = true;
      const schId = this.authService.getSchoolId();
      this.newService.getNews(id, schId ?? '').subscribe(
        (news) => {
          if (news) {
            this.new = news[0];
          }
        },
        (err) => {
          console.log('Hiba');
          this.toastrService.error(err.message);
        }
      );
    }
  }

  isEditMode(): boolean {
    return this.mainService.isEditMode();
  }

  onSave(news: News) {
    if (this.isUpdate) {
      this.newService.updateNews(news).subscribe((n) => {
        if (this.mainService.isEditMode()) this.mainService.toggleMode();
        this.toastrService.success('Sikeres módosítás!');
      });
    } else {
      news._id = '';
      this.newService.createNews(news).subscribe((n) => {
        if (this.mainService.isEditMode()) this.mainService.toggleMode();
        this.toastrService.success('Sikeres hozzáadás!');
      });
    }
    const schoolUrl = this.authService.getSchoolUrl();
    this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}`);
  }

  onDelete(news: News) {
    this.newService.deleteNews(news).subscribe((news) => {
      this.toastrService.success('Sikeres törlés!');
      const schoolUrl = this.authService.getSchoolUrl();
      this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}`);
    });
  }

  onCancel(news: News) {
    const schoolUrl = this.authService.getSchoolUrl();
    if (news._id === 'tmp') {
      this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}`);
      return;
    }
    this.mainService.toggleMode();
  }

  onPublish(news: News) {
    this.newService.publishNews(news, true).subscribe((n) => {
      this.new = n;
      this.toastrService.success('Közzétette a hírt!');
    });
  }

  onUnPublish(news: News) {
    this.newService.publishNews(news, false).subscribe((n) => {
      this.new = n;
      this.toastrService.success('Elrejtette a hírt!');
    });
  }
}
