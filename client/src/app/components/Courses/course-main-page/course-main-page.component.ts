import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { CourseService } from 'src/app/services/course.service';
import { MainService } from 'src/app/services/main.service';
import { Course, emptyCourse } from 'src/models/Course';

@Component({
    selector: 'app-course-main-page',
    templateUrl: './course-main-page.component.html',
    styleUrls: ['./course-main-page.component.scss']
})
export class CourseMainPageComponent implements OnInit {
    course: Course = emptyCourse;
    canEdit: boolean;
    constructor(
        private courseService: CourseService,
        private route: ActivatedRoute,
        private mainService: MainService,
        private authService: AuthService,
    ) {}

    ngOnInit(): void {
        this.canEdit = this.authService.isAdmin();
        this.route.paramMap.subscribe(params => {
            const _id = params.get('id') ?? '';
            this.courseService.getCourse(_id).subscribe(course => {
                this.course = course;
                this.canEdit = this.courseService.isTeacher;
            });
        });
    }

    isEditMode(): boolean {
        return this.mainService.isEditMode();
    }

    // isAllowedToEdit(): boolean {
        // if (this.authService.isAdmin()){
        //     return true;
        // }
        // const userId = this.authService.getUserId();
        // this.courseService.getCourseRole(this.course, userId).subscribe(role => {
        //     return role.
        // });
        // return false;
    // }
}
