import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSegment } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { AuthService } from 'src/app/services/auth.service';
import { CourseService } from 'src/app/services/course.service';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { Course, emptyCourse } from 'src/models/Course';
import { Nav } from 'src/models/Nav';

@Component({
    selector: 'app-course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.scss']
})

export class CourseComponent implements OnInit, OnDestroy {
    private _id: string = '';
    course: Course = emptyCourse;
    private baseUrl: string|null = null;
    navs: Nav[] = [];

    constructor(
        private route: ActivatedRoute,
        private courseService: CourseService,
        private toastrService: ToastrService,
        private navigationService: NavigationService,
        private mainService: MainService,
        private authService: AuthService,
    ) {
        this.route.url.subscribe(url => this.baseUrl = url.join('/'));
        this.navs = [
            { name: 'Résztvevők', navigationLink: this.baseUrl + '/felhasznalok', active: false},
            { name: 'Főoldal', navigationLink: this.baseUrl ?? 'null', active: true},
            { name: 'Fájlok', navigationLink: this.baseUrl + '/fajlok', active: false},
            { name: 'Kvízek', navigationLink: this.baseUrl + '/kvizek', active: false}
        ];
    }

    ngOnInit(): void {
        this.route.paramMap.subscribe(params => {
            this._id = params.get('id') ?? '';
            this.courseService.getCourse(this._id).subscribe(res => {
                this.course = res;
                const userId = this.authService.getUserId();
                const isAdmin = this.authService.isAdmin();
                this.courseService.isTeacher = isAdmin || res.teachers.some((t: any) => t._id === userId);
                this.courseService.isStudent = res.students.some((t:any) => t._id === userId);
                this.mainService.setAllowedToEdit(this.courseService.isTeacher);
            }, 
            err => {
                if(err.status === 401){
                    this.toastrService.info('A megadott kurzus megtekintéséhez nincs joga!');
                }
                else {
                    this.toastrService.error('A megadott kurzus nem létezik!');
                }
                this.navigationService.deleteLast();
                this.navigationService.navigate(AppRoutes.courses);
            });
        });
    }

    ngOnDestroy(): void {
        this.mainService.setAllowedToEdit(false);
    }

    updateCourse(): void {
        if (this.course != null){
            this.courseService.updateCourse(this.course).subscribe(res => {
                this.course = res;
                console.log(res);
            });
        }else{
            this.toastrService.error('A változtatások mentése nem hajtható végre!');
        }
    }
}
