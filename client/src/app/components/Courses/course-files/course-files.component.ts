import { Component, OnInit } from '@angular/core';
import { MatIcon } from '@angular/material/icon';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';
import { CourseService } from 'src/app/services/course.service';
import { MainService } from 'src/app/services/main.service';
import { Attachment } from 'src/models/Course';
import { saveAs } from 'file-saver';

@Component({
    selector: 'app-course-files',
    templateUrl: './course-files.component.html',
    styleUrls: ['./course-files.component.scss']
})
export class CourseFilesComponent implements OnInit {

    private imgs: string[] = [ 'jpg', 'png', ];
    newFile: File | null = null;
    attachments: Attachment[] = [];

    constructor(
        private mainService: MainService,
        private toastrService: ToastrService,
        private authService: AuthService,
        private courseService: CourseService,
    ) { }

    ngOnInit(): void {
        this.courseService.getAttachments().subscribe(res => {
            console.log(res);
            
            this.attachments = res.map(a => {
                a.panelOpen = false;
                return a;
            });
        });
    }

    isEditMode(): boolean {
        return this.mainService.isEditMode();
    }

    onClick(): void {

    }

    onDownload(file:Attachment){
        this.courseService.downloadAttachment(file._id).subscribe(data => {
            let downloadURL = window.URL.createObjectURL(data);
            saveAs(downloadURL);
            this.toastrService.info(file.fileName + ' letöltése megkezdődött!');
        },
        error => {
            console.log(error);
            if (error.status === 404){
                this.toastrService.error('A fájl nem található!');
            }
        });
    }

    onDelete(file:Attachment): void {
        this.courseService.deleteAttachment(file._id).subscribe(res => {
            this.attachments = this.attachments.filter(a => a._id !== res);
            this.toastrService.warning(file.fileName + ' törölve!');
        });
    }

    getFileExtensionImage(file: any): string {
        if (this.imgs.includes(file.extension)){
            return 'image';
        }
        if (file.extension == 'txt' || file.extension == 'pdf'){
            return 'insert_drive_file';
        }
        return 'attachment';
    }

    opened(attachment:Attachment): void {
        // this.courseService.getAttachmentContent(attachment._id).subscribe(res => {
        //     console.log(res);
        //     attachment.content = res;
        // });
        this.attachments.forEach((file:any) => {
            file.panelOpen = false;
        });
        attachment.panelOpen = true;
    }

    closed(file:any): void {
        file.panelOpen = false;
    }

    getFileDate(file: Attachment): string {
        if (!file || !file.createdAt){
            return '-';
        }
        // console.log('FILE', typeof(file.createdAt));
        const dateTime = Date.parse(file.createdAt.toString());
        const date = new Date(dateTime);
        return date.toLocaleDateString('hu-Hu');
    }

    isAdminOrTeacher(): boolean {
        return this.authService.isAdmin() || this.courseService.isTeacher;
    }

    onFileSelected(event:any) {
        const file:File = event.target.files[0];
        if (file){
            this.newFile = file;
        }else{
            this.toastrService.error('Nem sikerült a fájl kiválasztása!');
        }
    }

    uploadFile(){
        if (this.newFile){
            const formData = new FormData();
            formData.set('attachment', this.newFile, this.newFile.name);
            this.courseService.uploadAttachment(formData).subscribe(res => {
                this.attachments.push(res);
                this.toastrService.success('Sikeresen hozzáadta a fájlt!')
            });
        }
    }
}
