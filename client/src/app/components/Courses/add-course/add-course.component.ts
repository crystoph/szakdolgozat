import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { CourseService } from 'src/app/services/course.service';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { Course, emptyCourse } from 'src/models/Course';

@Component({
    selector: 'app-add-course',
    templateUrl: './add-course.component.html',
    styleUrls: ['./add-course.component.scss']
})
export class AddCourseComponent implements OnInit {
    @Input() course: Course = emptyCourse;
    @Input() isUpdate: Boolean = false;
    private tmpCourse: Course = emptyCourse;

    constructor(
        private courseService: CourseService,
        private toastrService: ToastrService,
        private navigationService: NavigationService,
        private mainService: MainService,
    ){ }

    ngOnInit(): void {
        this.tmpCourse = {...this.course};
    }

    onAddCourse() {
        if (!this.course.name){
            this.toastrService.error('A kurzus nevének megadása kötelező!');
            return;
        }
        if (!this.isUpdate){
            this.courseService.createCourse(this.course).subscribe((res:any) => {
                this.toastrService.success('A kurzus sikeresen hozááadásra került!');
                this.navigationService.navigate(AppRoutes.courses + '/' + res.course._id);
            },
                err => {
                    console.log(err);
                    this.toastrService.error(err.error.message);
                });
        }
        else {
            this.courseService.updateCourse(this.course).subscribe((res:any) => {
                this.toastrService.success('Sikeres módosítás!');
                this.mainService.toggleMode();
            },
                err => {
                    console.log(err);
                    this.toastrService.error(err.error.message);
                });
        }
    }

    onCancel(): void {
        if (!this.isUpdate){
            this.navigationService.navigate(AppRoutes.courses);
            this.course = emptyCourse;    
        }else {
            this.mainService.toggleMode();
            this.course.name = this.tmpCourse.name;
            this.course.description = this.tmpCourse.description;
            this.isUpdate = false;
        }
    }

}
