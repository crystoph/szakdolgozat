import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { filter } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { CourseService } from 'src/app/services/course.service';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { CourseUser } from 'src/models/User';

@Component({
    selector: 'app-course-user-list',
    templateUrl: './course-user-list.component.html',
    styleUrls: ['./course-user-list.component.scss']
})
export class CourseUserListComponent implements OnInit {
    users: CourseUser[] = [];
    _id: string = '';
    addingUsers: boolean = false;
    addUsers: CourseUser[] = [];

    constructor(
        private route: ActivatedRoute,
        private courseService: CourseService,
        private toastrService: ToastrService,
        private mainService: MainService,
        private authService: AuthService
    ) { }

    ngOnInit(): void {
        this.route.parent?.paramMap.subscribe(params => {
            this._id = params.get('id') ?? '';
            this.courseService.getUsers(this._id).subscribe(users => {
                this.users = users;
            },
            err => {
                this.toastrService.error('Hiba!');
                console.log(err);
            });
        });
    }

    wouldAddUser(): void {
        this.courseService.getUsersNotInCourse(this._id).subscribe(res => {
            res.map(u => u.userRole = 'Student');
            this.addUsers = res;
        });
        this.addingUsers = !this.addingUsers;
    }

    onSubmit(): void {
        const selectedUsers = this.addUsers.filter(u => u.selected);
        if (selectedUsers.length === 0){
            this.toastrService.info('Jelöljön ki felhasználókat a hozzárendelés előtt!');
            return;
        }
        this.courseService.addUsers(this._id, selectedUsers).subscribe(addedUsers => {
            const addedUserIds = addedUsers.map((a:CourseUser) => a._id);
            this.addUsers = this.addUsers.filter(oldUser => !addedUserIds.includes(oldUser._id));
            addedUsers.forEach((a:CourseUser) => {
                a.selected = false;
                this.users.push(a);
            });
            this.toastrService.success('Sikeres hozzárendelés!');
        }, err => {
            this.toastrService.error(err.error);
        });
    }

    refreshUsers(): void {
        this.courseService.getUsers(this._id).subscribe(users => {
            this.users = users;
            this.toastrService.success('A lista frissült!');
        },
        err => {
            this.toastrService.error('Nem sikerült újratölteni a résztvevők listáját!');
            console.log(err);
        });
    }

    refreshPossibleUsers(): void {
        this.courseService.getUsersNotInCourse(this._id).subscribe(users => {
            this.addUsers = users;
            this.toastrService.success('A lista frissült!');
        },
        err => {
            this.toastrService.error('Nem sikerült újratölteni a lehetséges résztvevők listáját!');
            console.log(err);
        });
    }

    onDelete(): void {
        const selectedUsers = this.users.filter(u => u.selected);
        if (selectedUsers.length === 0){
            this.toastrService.info('Jelöljön ki felhasználókat a törlés előtt!');
            return;
        }
        this.courseService.deleteUsers(this._id, selectedUsers.map(u => u._id)).subscribe((userIds:string[]) => {
            this.users = this.users.filter(user => !userIds.includes(user._id));
            this.toastrService.success(userIds.length + ' db felhasználó hozzárendelése megszüntetve!');
        },
        err => {
            this.toastrService.error(err.message);
            console.log(err);
        });
    }

    isAdmin(): boolean {
        return this.authService.isAdmin();
    }

    isEditMode(): boolean {
        return this.mainService.isEditMode();
    }
}
