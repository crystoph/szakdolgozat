import { Component, ViewChild, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { CourseService } from 'src/app/services/course.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { Course } from 'src/models/Course';
import { MainService } from 'src/app/services/main.service';
@Component({
    selector: 'app-coursespage',
    templateUrl: './coursespage.component.html',
    styleUrls: ['./coursespage.component.scss']
})
export class CoursespageComponent implements OnInit {
    courses: Course[] = [];
    newCourseName: string = '';
    constructor(
        private navigationService: NavigationService,
        private courseService: CourseService,
        private toastrService: ToastrService,
        private mainService: MainService,
    ) { }

    ngOnInit(): void {
        this.getAllCourses(true);
    }

    getAllCourses(init: boolean = false): void {
        this.courseService.getAllCourses().subscribe(res => {
            this.courses = res;
            if (!init){
                this.toastrService.success('A lista frissült!');
            }
        },
        err => {
            console.log(err.error.message);
            this.toastrService.error('Hiba történt!');
        });
    }

    onAddCourse(): void {
        this.navigationService.navigate(AppRoutes.addCourse);
    }

    onCourseClick(_id: string): void {
        this.navigationService.navigate(AppRoutes.courses + '/' + _id);
    }

    onDeleteCourse(_id:string): void {
        const course = this.courses.find(c => c._id == _id);
        if (course !== undefined){
            this.courseService.deleteCourse(course).subscribe(res => {
                this.courses = this.courses.filter(c => c._id !== res._id);
            });
        }
    }

    isEditMode(): boolean {
        return this.mainService.isEditMode();
    }
}