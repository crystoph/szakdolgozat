import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/app.routes';
import { AuthService } from 'src/app/services/auth.service';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email: string = '';
  password: string = '';

  constructor(
    private authService: AuthService,
    private toastr: ToastrService,
    private navigationService: NavigationService
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    this.authService.login(this.email, this.password).subscribe((res) => {
      if (res.success) {
        this.toastr.success(res.message);
        this.authService.saveTokens(
          res.accessToken,
          res.refreshToken,
          res.user
        );
        this.navigationService.navigate(
          `${res.user.schoolUrl}/${AppRoutes.main}`
        );
      } else {
        this.toastr.error(res.message);
      }
    });
  }

  onRegistration() {
    this.navigationService.navigate(AppRoutes.register);
  }
}
