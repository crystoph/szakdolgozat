import { Component, Input, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { Nav } from 'src/models/Nav';
import { internalErrorMessage, internalWarningMessage } from 'src/app/helpers/HelperFunctions';
import { Router } from '@angular/router';
import { map } from 'rxjs';

@Component({
    selector: 'app-sidenav-list',
    templateUrl: './sidenav-list.component.html',
    styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {
    @Input() navs: Nav[] = [];
    private isBooted: boolean = false;

    constructor(
        private navigationService: NavigationService,
        private router: Router
    ) { }

    ngOnInit(): void {
        if (this.navs.length > 0){
            this.switchViews();
        } else {
            internalErrorMessage(this.constructor.name, 'No navigation links!');
        }
    }

    switchViews(): void {
        let nav:any;
        //link alapján
        if(this.navs.some(nav => nav.navigationLink === this.router.url)){
            nav = this.navs.find(nav => nav.navigationLink == this.router.url);
        }
        // active alapján
        if (this.navs.some(nav => nav.active)){
            nav = this.navs.find(nav => nav.active);
        }
        // vagy csak hogy legyen valami
        else if (this.navs.length > 0){
            nav = this.navs[0];
        }
        this.isBooted = true;
        this.navigationService.navigate(nav?.navigationLink ?? '');
        this.navs.map(n => n.active = n.navigationLink == nav?.navigationLink);
        console.log('selected nav', nav);
    }

    onClick(nav: Nav){
        this.navs.map(n => n.active = n === nav);
        this.switchViews();
    }
}
