import { Component, Input, OnInit } from '@angular/core';
import { AppRoutes } from 'src/app/app.routes';
import { AuthService } from 'src/app/services/auth.service';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { SchoolService } from 'src/app/services/school.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  positionType: string | undefined = undefined;

  constructor(
    private authService: AuthService,
    private mainService: MainService,
    private navigationService: NavigationService,
    private schoolService: SchoolService
  ) {}

  getUserName() {
    return this.authService.getUserName();
  }

  toggleMode(): void {
    this.mainService.toggleMode();
  }

  isEditMode(): boolean {
    return this.mainService.isEditMode();
  }

  ngOnInit(): void {
    // if(!this.schoolNameOrAppTitle)
    // {
    //     this.authService.loggedInUser.subscribe(u => {
    //         if (u){
    //             this.schoolNameOrAppTitle = u.schoolName;
    //             this.positionType = u.positionType;
    //         }
    //     });
    // }else{
    //     this.authService.saveSchoolName(this.schoolNameOrAppTitle);
    // }
  }

  main() {
    const schoolUrl = this.authService.getSchoolUrl();
    this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}`);
  }

  courses() {
    this.navigationService.navigate(AppRoutes.courses);
  }

  users() {
    this.navigationService.navigate(AppRoutes.users);
  }

  school() {
    this.navigationService.navigate(AppRoutes.school);
  }

  logout(): void {
    this.authService.logout();
    this.navigationService.navigateWithNoSave(AppRoutes.login);
  }

  login(): void {
    this.authService.saveSchoolName(null);
    this.navigationService.navigateWithNoSave(AppRoutes.login);
  }

  isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }

  goBack(): void {
    this.navigationService.goPrevPage();
  }

  getComponentName(): string {
    return this.navigationService.getComponentName();
  }

  getSchoolName(): string {
    return this.authService.getSchoolName() ?? environment.appName;
  }

  onUserUpdateClicked(): void {
    const userId = this.authService.getDecodedToken()?._id;
    this.navigationService.navigate(`${AppRoutes.users}/${userId}`);
  }

  allowedToEdit(): boolean {
    return this.mainService.getAllowedToEdit();
  }

  getProfilePicture(): string | null {
    return this.authService.getUserProfilePicture();
  }

  isUserAdmin(): boolean {
    return this.authService.isAdmin();
  }
}
