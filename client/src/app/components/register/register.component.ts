import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { NavigationService } from 'src/app/services/navigation.service';
import { SchoolService } from 'src/app/services/school.service';
import { emptyUser, User } from 'src/models/User';
import { emptySchool, School } from 'src/models/School';
@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    user: User = emptyUser;
    school: School = emptySchool;
    constructor(
        private schoolService: SchoolService,
        private toastr: ToastrService,
        private navigationService: NavigationService
    ) { }

    ngOnInit(): void {
    }

    onSubmit() {
        this.schoolService.addSchool(this.user, this.school)
        .subscribe((res) => {
            console.log(res);
            this.toastr.success(res.message);
            this.navigationService.navigate(AppRoutes.login);
        },
        (err) => {
            console.log(err);
            this.toastr.error(err.error.errors[0].msg);
        });
    }
}
