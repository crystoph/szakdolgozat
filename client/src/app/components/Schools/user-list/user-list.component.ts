import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { AppRoutes } from 'src/app/app.routes';
import { ToastrService } from 'ngx-toastr';
import { MainService } from 'src/app/services/main.service';
import { User } from 'src/models/User';
import {Clipboard} from '@angular/cdk/clipboard';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    users: User[] = [];
    displayedColumns = ['number', 'name', 'e-mail', 'methods'];
    searchValue: string = '';
    numberOfUsers: number = 1;

    constructor(
        private userService: UserService,
        private authService: AuthService,
        private navigationService: NavigationService,
        private toastrService: ToastrService,
        private mainService: MainService,
        public dialog: MatDialog,
        private clipBoard: Clipboard,
    ) { }

    ngOnInit(): void {
        const schoolId = this.authService.getSchoolId()??'';
        this.userService.getUsers(schoolId).subscribe(res => {
            this.users = res;
            let i = 0;
            this.users = this.users.map(function(e) { return {...e, number:i++}});
        });
    }

    onRefresh() {
        const schoolId = this.authService.getSchoolId()??'';
        this.userService.getUsers(schoolId).subscribe(res => {
            this.users = res;
            let i = 0;
            this.users = this.users.map(function(e) { return {...e, number:i++}});
            this.toastrService.success('A lista sikeresen frissült!');
        });
        console.log(this.authService.getDecodedToken());
    }

    onDeleteUser(user: User) {
        this.userService.deleteUser(user).subscribe(() => {
            this.users = this.users.filter(e => e._id !== user._id);
        });
    }

    onGetUser(user: User){
        this.navigationService.navigate(`${AppRoutes.users}/${user._id}`);
    }

    isEditMode(): boolean {
        return this.mainService.isEditMode();
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(UserListDialog, {
            width: '400px',
            data: this.numberOfUsers
        });

        dialogRef.afterClosed().subscribe(result => {
            this.numberOfUsers = result;
            if (this.numberOfUsers){
                this.generateUsers();
            }
        });
    }

    generateUsers(): void {
        const schoolId = this.authService.getSchoolId() ?? '';
        
        this.userService.generateUsers(this.numberOfUsers, schoolId)
            .subscribe(res => {
                const users = this.users.concat(res);
                this.users = users;
            },
            err => this.toastrService.error('Generálási hiba!'));
    }

    onCopyPassword(user: User): void {
        this.userService.getDefaultPassword(user).subscribe(res => {
            this.clipBoard.copy(res ?? '');
            if (res){
                this.toastrService.success('Vágólapra másolva!');
            }
        }, err => {
            this.toastrService.error(err.error);
        });
    }
}

@Component({
    selector: 'user-list-dialog',
    templateUrl: 'user-list-dialog.html',
    })
export class UserListDialog {

    constructor(
        public dialogRef: MatDialogRef<UserListDialog>,
        @Inject(MAT_DIALOG_DATA) public data: number) {}

    onNoClick(): void {
        this.dialogRef.close();
    }

}