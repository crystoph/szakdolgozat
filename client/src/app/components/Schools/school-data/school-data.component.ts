import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { SchoolService } from 'src/app/services/school.service';
import { emptySchool, School } from 'src/models/School';

@Component({
  selector: 'app-school-data',
  templateUrl: './school-data.component.html',
  styleUrls: ['./school-data.component.scss'],
})
export class SchoolDataComponent implements OnInit {
  school: School = emptySchool;
  emblem: string = '';

  constructor(
    private schoolService: SchoolService,
    private navigationService: NavigationService,
    private toastrService: ToastrService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    const id = this.authService.getSchoolId() ?? '';
    this.schoolService.getSchool(id).subscribe((res) => {
      this.school = res;
      this.emblem = this.school.emblem;
    });
  }

  goPrev() {
    this.navigationService.goPrevPage();
  }

  onSave() {
    this.schoolService.updateSchool(this.school).subscribe((res) => {
      this.school = res;
      this.toastrService.success('Sikeres módosítás!');
      this.authService.saveSchoolUrl(this.school.pageUrl);
    });
  }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const base64 = reader.result as string;
        this.emblem = base64;
      };
    } else {
      this.toastrService.error('Nem sikerült a fájl kiválasztása!');
    }
  }

  uploadFile() {
    this.school.emblem = this.emblem;
    if (this.school.emblem) {
      this.schoolService
        .uploadEmblem(this.school.emblem, this.school._id)
        .subscribe(
          (res) => {
            this.toastrService.success('Sikeres címer hozzáadás!');
          },
          (err) => {
            this.toastrService.error('Nem sikerült a feltöltés!');
          }
        );
    }
  }
}
