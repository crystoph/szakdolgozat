import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Nav } from 'src/models/Nav';

@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styleUrls: ['./school.component.scss']
})
export class SchoolComponent implements OnInit {
    navs: Nav[];
    baseUrl: string | null = null;

    constructor(
        private route: ActivatedRoute
    ) {
        this.route.url.subscribe(url => this.baseUrl = url.join('/'));
        this.navs = [
            { name: 'Iskola felhasználói', navigationLink: this.baseUrl + '/felhasznalok', active: false},
            { name: 'Iskola adatai', navigationLink: this.baseUrl + '/', active: true},
        ];
    }

    ngOnInit(): void {
        
    }
}
