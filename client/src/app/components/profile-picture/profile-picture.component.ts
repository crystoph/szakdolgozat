import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.scss']
})
export class ProfilePictureComponent implements OnInit {
    @Input() src: string | null = null;
    constructor() { }

    ngOnInit(): void {
        console.log('PROFILE INIT', this.src);
        if (!this.src || this.src === null){
            console.log('src is: ', this.src);
            this.src = '../../../assets/images/default-user-profile.avif';
        }
    }

}
