import { Component, Input, OnInit, Output } from '@angular/core';
import { emptyNews, News } from 'src/models/New';
import { NavigationService } from 'src/app/services/navigation.service';
import { AppRoutes } from 'src/app/app.routes';
import { AuthService } from 'src/app/services/auth.service';
import { MainService } from 'src/app/services/main.service';

@Component({
    selector: 'app-news-item',
    templateUrl: './news-item.component.html',
    styleUrls: ['./news-item.component.scss']
})
export class NewsItemComponent implements OnInit {
    @Input() new:News = emptyNews;

    constructor(
        private navigationService: NavigationService,
        private authService: AuthService,
    ) { }

    ngOnInit(): void {   
    }

    onNewClick(newItem: News) {
        if (this.new._id !== 'tmp'){
            const schoolUrl = this.authService.getSchoolUrl();
            this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}/${newItem._id}`);
        }
        if (newItem._id === 'tmp'){
            console.log('AddNewNew');
            const schoolUrl = this.authService.getSchoolUrl();
            console.log('schoolUrl:', schoolUrl);
            this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}/${this.new._id}`);
        }
    }

    onAddNew(newItem: News){
        if (newItem._id === 'tmp'){
            console.log('AddNewNew');
            const schoolUrl = this.authService.getSchoolUrl();
            console.log('schoolUrl:', schoolUrl);
            this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}/${this.new._id}`);
        }
    }
}
