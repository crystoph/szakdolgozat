import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { QuizzService } from 'src/app/services/quizz.service';
import { QuizListItem, SubmittedQuizListItem } from 'src/models/Quiz';
import { QuizStatus } from 'src/models/enums/Enums';

@Component({
  selector: 'app-submitted-quizzes',
  templateUrl: './submitted-quizzes.component.html',
  styleUrls: ['./submitted-quizzes.component.scss']
})
export class SubmittedQuizzesComponent implements OnInit {
    submitted: SubmittedQuizListItem[] = [];
    // quiz: QuizListItem;
    constructor(
        private quizService: QuizzService,
        private toastrService: ToastrService,
        private route: ActivatedRoute,
        private mainService: MainService,
        private navigationService: NavigationService,
    ) { }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        if (id){
            this.quizService.getSubmitted(id).subscribe(res => {
                console.log('submitted', res);
                this.submitted = res;
                // this.quiz = res[0];
            },
            err => {
                console.log('ERROR', err);
                this.toastrService.error('Hiba történt!');
            });
        }
        // else {
        //     this.navigationService.navigateWithNoSave(AppRoutes.courses);
        // }
    }

    onQuizClick(item: SubmittedQuizListItem){
        if (item.quiz.status === QuizStatus.STARTED){
            return;
        }
        // const id = this.route.snapshot.paramMap.get('id');
        this.navigationService.navigate(`${AppRoutes.quizzes}/${item.quiz._id}/${AppRoutes.submitted}/${item.user._id}`);
    }

    isEditMode(): boolean {
        return this.mainService.isEditMode();
    }

    public get QuizStatusEnum(){
        return QuizStatus;
    }
}
