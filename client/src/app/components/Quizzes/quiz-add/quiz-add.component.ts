import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { transformTimeSpan } from 'src/app/helpers/HelperFunctions';
import { AuthService } from 'src/app/services/auth.service';
import { CourseService } from 'src/app/services/course.service';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { QuizzService } from 'src/app/services/quizz.service';
import { AnswerType, QuizStatus } from 'src/models/enums/Enums';
import { emptyQuiz, Question, Quiz, getEmptyQuestion} from 'src/models/Quiz';

@Component({
    selector: 'app-quiz-add',
    templateUrl: './quiz-add.component.html',
    styleUrls: ['./quiz-add.component.scss']
})
export class QuizAddComponent implements OnInit, OnDestroy {
    @Input() quiz: Quiz;
    // @Input() isUpdate: boolean = false;
    minDate: Date = new Date();
    showAddMenu: Boolean = true;
    fromTimeStr: string = '';
    toTimeStr: string = '';
    display:any;
    interval:any;
    isTimeFlowsToPositive: boolean = true;
    private timeFromStart: number = 0;
    solveTimeMinutes:number;

    constructor(
        private navigationService: NavigationService,
        private mainService: MainService,
        private quizService: QuizzService,
        private toastrService: ToastrService,
        private courseService: CourseService,
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,

    ) {
    }

    ngOnDestroy(): void {
        clearInterval(this.interval);
    }

    private setQuizData(id: string) {
        const userId = this.authService.getUserId();
        if (this.courseService.isStudent){
            this.quizService.startQuiz(this.quizService.lastQuizId).subscribe(res => {
                this.quiz = res;
                if (this.quiz.startTime){
                    const startDateTime = new Date(this.quiz.startTime).getTime();
                    this.timeFromStart = Math.floor((new Date().getTime() - startDateTime) / 1000);
                }
                this.startTimer();
            }, err => {
                this.toastrService.error(err.error);
                const url = `${AppRoutes.courses}/${this.courseService.lastCourseId}/${AppRoutes.quizzes}`;
                this.navigationService.navigate(url);
            });
        }
        if (this.courseService.isTeacher || this.authService.isAdmin()){
            this.quizService.getQuiz(id, userId).subscribe(res => {
                this.quiz = res;
                this.fromTimeStr = this.getFullDateTime(res.canSolveFrom);
                this.toTimeStr = this.getFullDateTime(res.canSolveTo);
                if (res.solveTimeSeconds){
                    this.solveTimeMinutes = Math.floor(res.solveTimeSeconds/60);
                }
            });
        }
    }

    ngOnInit(): void {
        this.quiz = emptyQuiz;
        const id = this.activatedRoute.snapshot.paramMap.get('id');
        // console.log('init', id);
        if (!id){
            return;
        }
        this.setQuizData(id);
    }

    public get QuizStatusEnum(){
        return QuizStatus;
    }

    onDelete(): void {
        this.quizService.deleteQuiz(this.quiz).subscribe(res => {
            this.navigationService.navigateWithNoSave(`${AppRoutes.courses}/${this.courseService.lastCourseId}/${AppRoutes.quizzes}`);
        });
    }

    onCancel(): void {
        if (this.quiz._id === 'tmp'){
            this.navigationService.navigateWithNoSave(`${AppRoutes.courses}/${this.courseService.lastCourseId}/${AppRoutes.quizzes}`);
        }else{
            this.setQuizData(this.quiz._id);
        }
    }

    onSave(): void {
        this.quiz.courseId = this.courseService.lastCourseId;
        // if (this.quiz.timeRemaining){
        //     this.quiz.timeRemaining = Math.floor(this.quiz.timeRemaining);
        // }
        this.quiz.solveTimeSeconds = this.solveTimeMinutes * 60;
        // console.log('minutes', this.solveTimeMinutes);
        if (this.quiz._id && this.quiz._id !== 'tmp'){
            this.quizService.updateQuiz(this.quiz).subscribe(res => {
                this.quiz = res;
                this.toastrService.success('Sikeres módosítás!');
                this.mainService.toggleMode();
            },
            err => {
                this.toastrService.error(err.error);
            });
        }else if(this.quiz._id === 'tmp'){
            this.quizService.addQuiz(this.quiz).subscribe(res => {
                this.quiz = res;
                this.mainService.toggleMode();
                this.toastrService.success('Új kvízt hozott létre!');
            },
            res => {
                console.log(res);
                this.toastrService.error(res.error.errors[0].msg);
            });
        }
    }

    addEmptyQuestion(): void {
        this.showAddMenu = true;
    }

    onDeleteQuestion(question: Question): void {
        this.quiz.questions = this.quiz.questions.filter(q => q !== question);
        this.quiz.questions = this.quiz.questions.map(q => {
            if (q.ordinal > question.ordinal){
                q.ordinal = q.ordinal - 1;
            }
            return q;
        });
    }

    compare(a:number, b:number) {
        if (a < b) {
            return -1;
          }
          if (a > b) {
            return 1;
          }
          return 0;
    }

    sortQuestions() {
        this.quiz.questions.sort((q1:Question, q2:Question) => this.compare(q1.ordinal, q2.ordinal));
    }

    addEssayQuestion(): void {
        this.showAddMenu = false;
        this.quiz.questions.push(
            getEmptyQuestion(this.quiz.questions.length + 1, AnswerType.ESSAY)
        );
    }

    addSortQuestion(): void {
        this.showAddMenu = false;
        this.quiz.questions.push(
            getEmptyQuestion(this.quiz.questions.length + 1, AnswerType.SORT)
        );
    }

    addPickerQuestion(): void {
        this.showAddMenu = false;
        this.quiz.questions.push(
            getEmptyQuestion(this.quiz.questions.length + 1, AnswerType.PICKER)
        );
    }

    addConcreteQuestion(): void {
        this.showAddMenu = false;
        this.quiz.questions.push(
            getEmptyQuestion(this.quiz.questions.length + 1, AnswerType.CONCRETE)
        );
    }

    isEditMode(): Boolean {
        return this.mainService.isEditMode();
    }

    onPublicChanged(): void {
        this.quiz.isPublic = !this.quiz.isPublic;
        this.quizService.publicQuiz(this.quiz, this.quiz.isPublic).subscribe(res => {
            if(this.quiz.isPublic){
                this.toastrService.success('Közzétette a kvízt!');
            }else {
                this.toastrService.success('Elrejtette a kvízt!');
            }
        },
        err => {
            this.quiz.isPublic = !this.quiz.isPublic;
            this.toastrService.error(err.error);
        });
    }

    onSubmit(): void {
        this.quizService.submitQuiz(this.quiz).subscribe(res => {
            const url = `${AppRoutes.courses}/${this.courseService.lastCourseId}/${AppRoutes.quizzes}`;
            this.navigationService.navigateWithNoSave(url);
            this.toastrService.success('Sikeresen leadta megoldását!');
        }, err => {
            // console.log(err);
            this.toastrService.error(err.error);
        });
    }

    getFullDateTime(date: Date | null): string {
        if (!date){
            return 'bármeddig';
        }
        // const value = new Date(date);
        const value = date;
        if (!value || isNaN(value.valueOf())){
            return 'bármeddig';
        }
        return value.getFullYear().toString()
        + '-' + value.getMonth().toString()
        + '-' + value.getDay().toString()
        + ' ' + value.getHours().toString()
        + ':' + value.getMinutes().toString()
        + ':' + value.getSeconds().toString();
    }

    startTimer() {
        this.interval = setInterval(() => {
          if (this.quiz.timeRemaining) {
            this.quiz.timeRemaining--;
            if (this.quiz.timeRemaining >= 0){
                this.display = this.transform(this.quiz.timeRemaining);
            } else {
                this.display = 'A határidő lejárt.';
            }
          } else if (this.quiz.startTime){
            this.timeFromStart++;
            this.display = this.transform(this.timeFromStart);
            this.isTimeFlowsToPositive = false;
          }
        }, 1000);
      }

    transform(value: number, withExplanation: boolean = false): string {
        return transformTimeSpan(value);
    }

    public get isTeacher(){
        return this.courseService.isTeacher;
    }
}
