import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MainService } from 'src/app/services/main.service';
import { AnswerType } from 'src/models/enums/Enums';
import { getEmptyQuestion, Question, Answer, getEmptyAnswer } from 'src/models/Quiz';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
    @Input() question: Question = getEmptyQuestion(1, AnswerType.ESSAY);
    @Input() isViewMode: boolean = true;
    @Input() isCorrectMode: boolean = false;
    isEssay: Boolean = false;
    isPicker: Boolean = false;
    isSort: Boolean = false;
    isConcrete: Boolean = false;
    @Output() questionDeleteEvent = new EventEmitter<Question>();
    // answers: Answer[] = [];
    constructor(
        private mainService: MainService,
    ) {}

    ngOnInit(): void { 
        // if (this.question.answers = []){
        //     this.question.answers = getEmptyAnswer(this.question.type);
        // }
        if(this.question.type === AnswerType.ESSAY){
            this.isEssay = true;
        }
        if(this.question.type === AnswerType.PICKER){
            this.isPicker = true;
        }
        if(this.question.type === AnswerType.SORT){
            this.isSort = true;
        }
        if(this.question.type === AnswerType.CONCRETE){
            this.isConcrete = true;
        }
        // console.log(this.question.type);
    }

    onDeleteQuestion(question: Question){
        this.questionDeleteEvent.emit(question);
    }

    onAddAnswer(question: Question): void {
        question.answers.push({
            selected: false,
            text: '',
            correct: false,
            hover: false,
            ordinal: question.answers.length + 1,
            // userAnswerId: '',
        });
    }

    onDeleteAnswer(answer: Answer): void {
        const lower = this.question.answers.filter(a => a.ordinal < answer.ordinal);
        const upper = this.question.answers.filter(a => a.ordinal > answer.ordinal);
        upper.forEach(a => a.ordinal = a.ordinal - 1);
        this.question.answers = Array.prototype.concat(lower, upper);
    }


    compare(a:number, b:number) {
        if (a < b) {
            return -1;
          }
          if (a > b) {
            return 1;
          }
          return 0;
    }

    onMoveAnswer(ordinal: number, direction: number){
        if (ordinal + direction > 0 && ordinal + direction <= this.question.answers.length){
            const neighbourAnswer = this.question.answers.find(a => a.ordinal === ordinal + direction);
            const thisAnswer = this.question.answers.find(a => a.ordinal === ordinal);
            
            if (neighbourAnswer !== undefined && thisAnswer !== undefined){
                thisAnswer.ordinal = neighbourAnswer.ordinal;
                neighbourAnswer.ordinal = ordinal;
                this.question.answers = this.question.answers.filter(a => {
                    return a.ordinal !== thisAnswer.ordinal && a.ordinal !== neighbourAnswer.ordinal;
                });
                this.question.answers.push(thisAnswer);
                this.question.answers.push(neighbourAnswer);
            }            
        }

        this.question.answers = this.question.answers.sort((a, b) => this.compare(a.ordinal, b.ordinal));
    }

    // onMoveDownAnswer(answer: Answer){
    //     if (answer.ordinal + 1 <= this.question.answers.length){
    //         answer.ordinal = answer.ordinal + 1;
    //     }
    //     this.question.answers = this.question.answers.sort((a, b) => this.compare(a.ordinal, b.ordinal));
    // }

    isEditMode(): boolean {
        return this.mainService.isEditMode() && !this.isViewMode;
    }

    isOnlyEditMode(){
        return this.mainService.isEditMode();
    }

    onAnswerCorrect(question: Question, isCorrect: boolean){
        if (isCorrect){
            question.earnedReward = question.reward;
        }else {
            question.earnedReward = 0;
        }
    }
}
