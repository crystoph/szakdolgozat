import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { CourseService } from 'src/app/services/course.service';
import { MainService } from 'src/app/services/main.service';
import { NavigationService } from 'src/app/services/navigation.service';
import { QuizzService } from 'src/app/services/quizz.service';
import { QuizListItem } from 'src/models/Quiz';
import { QuizStatus } from 'src/models/enums/Enums';

@Component({
    selector: 'app-quiz-list',
    templateUrl: './quiz-list.component.html',
    styleUrls: ['./quiz-list.component.scss']
})
export class QuizListComponent implements OnInit {
    
    quizzes: QuizListItem[] = [];

    constructor(
        private navigationService: NavigationService,
        private mainService: MainService,
        private quizzService: QuizzService,
        private courseService: CourseService,
        private toastrService: ToastrService,
    ) { }

    ngOnInit(): void {
        const courseId = this.courseService.lastCourseId;
        this.quizzService.getAllQuiz(courseId).subscribe(quizzes => {
            quizzes.forEach(q => {
                if (q.canSolveFrom){
                    const parsed = Date.parse(q.canSolveFrom.toString())
                    q.canSolveFrom = new Date(parsed);
                }
                if (q.canSolveTo){
                    const parsed = Date.parse(q.canSolveTo.toString())
                    q.canSolveTo = new Date(parsed);
                }
                // console.log(q.status === this.QuizStatusEnum.PUBLISHED);
                // console.log(q.status, this.QuizStatusEnum.PUBLISHED);
            });
            this.quizzes = quizzes;
        });
    }

    public get QuizStatusEnum(){
        return QuizStatus;
    }

    public get isStudent(): boolean {
        console.log('stud', this.courseService.isStudent);
        return this.courseService.isStudent;
    }

    public get isTeacher(): boolean {
        console.log('teach', this.courseService.isTeacher);
        return this.courseService.isTeacher;
    }

    onAddQuiz() : void {
        this.navigationService.navigateWithBase('ujkviz');
    }

    isEditMode(): Boolean {
        return this.mainService.isEditMode();
    }

    onQuizClick(quiz: QuizListItem){
        // if (quiz.status === QuizStatus.SUBMITTED){
        //     this.toastrService.info('A kvíz lezárásáig nem tekintheti meg az eredményeket!');
        //     return;
        // }
        this.quizzService.lastQuizId = quiz._id;
        this.navigationService.navigateWithBase('/' + quiz._id);
    }

    onQuizDelete(quizId: string){
        this.quizzService.deleteQuizById(quizId).subscribe(id => {
            this.quizzes = this.quizzes.filter(q => q._id !== id);
        });
    }

    getFullDateTime(value: Date | null): string | null{
        if (!value || isNaN(value.valueOf())){
            return null;
        }
        const dateTime = Date.parse(value.toString());
        const date = new Date(dateTime);
        const dateStr = date.toLocaleDateString('hu-Hu');
        const timeStr = date.toLocaleTimeString('hu-Hu');
        return `${dateStr} ${timeStr}`;
    }

    onQuizCorrection(quizId: string){
        this.quizzService.submittedOf = quizId;
        this.navigationService.navigate(`${AppRoutes.quizzes}/${quizId}/${AppRoutes.submitted}`);
    }
}