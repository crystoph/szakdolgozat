import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { transformTimeSpan } from 'src/app/helpers/HelperFunctions';
import { MainService } from 'src/app/services/main.service';
import { QuizzService } from 'src/app/services/quizz.service';
import { Question, Quiz, emptyQuiz } from 'src/models/Quiz';
import { QuizStatus } from 'src/models/enums/Enums';

@Component({
  selector: 'app-quizz-view',
  templateUrl: './quizz-view.component.html',
  styleUrls: ['./quizz-view.component.scss']
})
export class QuizzViewComponent implements OnInit {
    quiz: Quiz = emptyQuiz;

    constructor(
        private mainService: MainService,
        private quizService: QuizzService,
        private route: ActivatedRoute,
        private toastrService: ToastrService,
    ) { }

    private setQuiz(){
        const quizId = this.route.snapshot.paramMap.get('id');
        const userId = this.route.snapshot.paramMap.get('userId');
        if (!quizId || !userId){
            return;
        }
        this.quizService.getUserAnswer(quizId, userId).subscribe(res => {
            this.quiz = res;
        }, (err) => {
            console.log(err);
            // this.toastrService.info(err.error);
            // const url = `${AppRoutes.courses}/${this.courseService.lastCourseId}/${AppRoutes.quizzes}`;
            // this.navigationService.navigate(url);
        });
    }

    ngOnInit(): void {
        this.setQuiz();
    }

    isEditMode(): boolean {
        return this.mainService.isEditMode();
    }

    public get QuizStatusEnum(){
        return QuizStatus;
    }

    onClose(){
        if (this.quiz.questions.some(q => q.earnedReward === null)){
            this.toastrService.error('Értékeljen minden kérdést lezárás előtt!');
            return;
        }
        this.quizService.closeQuiz(this.quiz.userAnswerId??'', this.quiz.questions).subscribe(res => {
            this.toastrService.success('A kvíz lezárásra került!');
            this.setQuiz();
        });
    }

    getTimeSpan(){
        if (this.quiz.solveTimeSeconds){
            return transformTimeSpan(this.quiz.solveTimeSeconds, true);
        }
        return ' - ';
    }

    isQuestionCorrectMode(q: Question){
        return q.earnedReward === null;
    }
}
