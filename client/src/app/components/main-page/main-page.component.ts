import { Component, OnInit, ViewChild } from '@angular/core';
import { NewService } from 'src/app/services/new.service';
import { AuthService } from 'src/app/services/auth.service';
import { MainService } from 'src/app/services/main.service';
import { News, emptyNews } from 'src/models/New';
import { ActivatedRoute } from '@angular/router';
import { SchoolService } from 'src/app/services/school.service';
import { ToastrService } from 'ngx-toastr';
import { AppRoutes } from 'src/app/app.routes';
import { NavigationService } from 'src/app/services/navigation.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
    news: News[][] = [][0];
    pageNews: News[][] = [][0];
    pageIndex: number = 0;
    columns: number = 3;
    rows: number = 2;
    pageSize: number = 2;
    // @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    constructor(
        private newService: NewService,
        private authService: AuthService,
        private mainService: MainService,
        private route: ActivatedRoute,
        private schoolService: SchoolService,
        private toastrService: ToastrService,
        private navigationService: NavigationService,
    ) { }

    ngOnInit(): void {
        const schoolUrl = this.route.snapshot.paramMap.get('schoolUrl');
        if (schoolUrl){
            this.schoolService.getSchoolByUrl(schoolUrl ?? '').subscribe(school => {
                this.authService.saveSchoolName(school.schoolName);
                this.authService.saveSchoolUrl(schoolUrl);
                this.authService.saveSchoolId(school._id);
                this.newService.getAllNews(school._id ?? '').subscribe((news) => {
                    this.news = this.newsTo2D(news);
                    this.getData({pageIndex: this.pageIndex, pageSize: this.pageSize, length: this.news.length});
                }, err => {
                    this.toastrService.error(err.message);
                });
            },
            schoolErr => {
                if (schoolErr.status === 404){
                    this.toastrService.error('Nem találjuk a megadott iskolát!');
                }else {
                    this.toastrService.error(schoolErr.message);
                    console.log(schoolErr);
                }
            });
        }
        else {
            const mySchoolId = this.authService.getSchoolId();
            this.newService.getAllNews(mySchoolId ?? '').subscribe((news) => {
                this.news = this.newsTo2D(news);
            });
        }
    }

    getStartIndex(){
        return (this.pageIndex)*this.rows;
    }

    getEndIndex(){
        return (this.pageIndex+1)*this.rows + 1;
    }

    newsTo2D(news: News[]): News[][] {
        // const rowCount = Math.ceil(news.length/3);
        console.log('length: ', length);
        let result: News[][] = [];
        let rowInd: number = 0;
        let i = 1;
        for(; i<=news.length; i++){
            rowInd = Math.floor((i-1)/this.columns);
            if(i % this.columns === 1){
                result[rowInd] = [];
            }
            result[rowInd].push(news[i-1]);
            // console.log(`rowind: ${rowInd}, title: ${news[i-1].title}`);
        }
        // console.log('result', result);
        return result;
    }

    canAdd(): boolean {
        return this.authService.isAdmin() && this.mainService.isEditMode()
            // && Math.floor(i/3) === Math.floor(this.news.length/3)
            ;
    }

    addNewNews() {
        console.log('AddNewNew');
        const localNew: News = emptyNews;
        localNew.title = 'Cím';
        localNew.description = 'Leírás';
        localNew._id = 'tmp';
        const schoolUrl = this.authService.getSchoolUrl();
        this.navigationService.navigate(`${schoolUrl}/${AppRoutes.main}/${localNew._id}`);
    }

    getItemCount(){
        return Math.ceil(this.news?.length / this.pageSize);
    }

    getData(obj: PageEvent) {
        this.pageIndex = obj.pageIndex;
        let index=0,
            startingIndex=obj.pageIndex * this.rows,
            endingIndex=startingIndex + this.rows;
    
        this.pageNews = this.news.filter(() => {
          index++;
          return (index > startingIndex && index <= endingIndex) ? true : false;
        });
      }
}