import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PageNotFoundComponent } from 'src/app/components/page-not-found/page-not-found.component';
import { MainPageComponent } from 'src/app/components/main-page/main-page.component';
import { LoginComponent } from 'src/app/components/login/login.component';
import { RegisterComponent } from 'src/app/components/register/register.component';
import { UserListComponent } from 'src/app/components/Schools/user-list/user-list.component';
import { AppRoutes } from 'src/app/app.routes';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { AdminGuard } from 'src/app/guards/admin.guard';
import { CoursespageComponent } from 'src/app/components/Courses/coursespage/coursespage.component';
import { CourseComponent } from 'src/app/components/Courses/course/course.component';
import { CourseResolver } from 'src/app/resolvers/course.resolver';
import { AddCourseComponent } from '../../components/Courses/add-course/add-course.component';
import { CourseUserListComponent } from 'src/app/components/Courses/course-user-list/course-user-list.component';
import { CourseMainPageComponent } from 'src/app/components/Courses/course-main-page/course-main-page.component';
import { CourseFilesComponent } from 'src/app/components/Courses/course-files/course-files.component';
import { QuizListComponent } from 'src/app/components/Quizzes/quiz-list/quiz-list.component';
import { QuizAddComponent } from 'src/app/components/Quizzes/quiz-add/quiz-add.component';
import { SchoolComponent } from 'src/app/components/Schools/school/school.component';
import { UserComponent } from 'src/app/components/user/user.component';
import { SchoolDataComponent } from 'src/app/components/Schools/school-data/school-data.component';
import { SubmittedQuizzesComponent } from 'src/app/components/Quizzes/submitted-quizzes/submitted-quizzes.component';
import { QuizzViewComponent } from 'src/app/components/Quizzes/quizz-view/quizz-view.component';
import { NewPageComponent } from 'src/app/components/new-page/new-page.component';
import { TeacherGuard } from 'src/app/guards/teacher.guard';

const routes: Routes = [
    //Default routes
    {
        path:'',
        component: LoginComponent
    },
    { //Allowanonymus
        path:AppRoutes.login,
        component: LoginComponent
    },
    {
        path: AppRoutes.register,
        component: RegisterComponent
    },
    {
        path: `:schoolUrl/${AppRoutes.main}`,
        component: MainPageComponent,
    },
    {
        path: `:schoolUrl/${AppRoutes.main}/:newId`,
        component: NewPageComponent
    },
    { //Admin only routes
        path: '',
        canActivateChild: [AdminGuard],
        children:
        [
            {
                path: AppRoutes.addCourse,
                component: AddCourseComponent
            },
            {
                path: AppRoutes.school,
                component: SchoolComponent,
                children: [
                    {
                        path: AppRoutes.users,
                        component: UserListComponent
                    },
                    {
                        path: '',
                        component: SchoolDataComponent,
                    },
                ]
            },
        ]
    },
    {
        path: '',
        canActivateChild: [TeacherGuard],
        children: [
            {
                path: `${AppRoutes.quizzes}/:id/${AppRoutes.submitted}`,
                component: SubmittedQuizzesComponent,
            },
            {
                path: `${AppRoutes.quizzes}/:id/${AppRoutes.submitted}/:userId`,
                component: QuizzViewComponent,
            },
        ]
    },
    { //Authorized only routes
        path:'',
        canActivateChild: [AuthGuard],
        children:
        [
            {
                path: AppRoutes.courses,
                component: CoursespageComponent,
            },
            {
                path: AppRoutes.courses + '/:id',
                component: CourseComponent,
                children: [
                    {
                        path: 'felhasznalok',
                        component: CourseUserListComponent
                    },
                    {
                        path: '',
                        component: CourseMainPageComponent
                    },
                    {
                        path: 'fajlok',
                        component: CourseFilesComponent
                    },
                    {
                        path: AppRoutes.quizzes,
                        component: QuizListComponent,
                    },
                    {
                        path: AppRoutes.quizzes + '/ujkviz',
                        component: QuizAddComponent
                    },
                    {
                        path: AppRoutes.quizzes + '/:id',
                        component: QuizAddComponent,
                    },
               ]
            },
            {
                path:  AppRoutes.users + '/:id',
                component: UserComponent
            }
        ]
    },
    {
        path:'**',
        component: PageNotFoundComponent
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class RoutingModule { }