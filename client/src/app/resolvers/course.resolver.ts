import { Injectable } from '@angular/core';
import {
    Router, Resolve,
    RouterStateSnapshot,
    ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Course } from 'src/models/Course';

import { CourseService } from '../services/course.service';

@Injectable({
    providedIn: 'root'
})
export class CourseResolver implements Resolve<Course | null> {
    constructor(private courseService: CourseService){}

    resolve(route: ActivatedRouteSnapshot): Observable<Course | null> {
        const _id = route.paramMap.get('id');
        if (!_id){
            return of(null);
        }
        return this.courseService.getCourse(_id);
    }
}
