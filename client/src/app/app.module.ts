import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { MatTableModule } from '@angular/material/table';
import { QuillModule } from 'ngx-quill';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import { AppComponent } from './app.component';
import { RoutingModule } from './modules/router/router.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { UserListComponent, UserListDialog } from './components/Schools/user-list/user-list.component';
import { ForbiddenPageComponent } from './components/forbidden-page/forbidden-page.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { JwtModule } from '@auth0/angular-jwt';
import { CoursespageComponent } from './components/Courses/coursespage/coursespage.component';
import { CourseComponent } from './components/Courses/course/course.component';
import { AddCourseComponent } from './components/Courses/add-course/add-course.component';
import { NgbModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { CourseUserListComponent } from './components/Courses/course-user-list/course-user-list.component';
import { SidenavListComponent } from './components/sidenav-list/sidenav-list.component';
import { CourseMainPageComponent } from './components/Courses/course-main-page/course-main-page.component';
import { CourseFilesComponent } from './components/Courses/course-files/course-files.component';
import { SafeUrlPipe } from './pipes/safe-file-pipe.pipe';
import { QuizListComponent } from './components/Quizzes/quiz-list/quiz-list.component';
import { QuizAddComponent } from './components/Quizzes/quiz-add/quiz-add.component';
import { QuestionComponent } from './components/Quizzes/question/question.component';
import { SchoolComponent } from './components/Schools/school/school.component';
import { UserComponent } from './components/user/user.component';
import { SchoolDataComponent } from './components/Schools/school-data/school-data.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule,  NgxMatTimepickerModule  } from '@angular-material-components/datetime-picker';
import { QuizzViewComponent } from './components/Quizzes/quizz-view/quizz-view.component';
import { SubmittedQuizzesComponent } from './components/Quizzes/submitted-quizzes/submitted-quizzes.component';
import { NewPageComponent } from './components/new-page/new-page.component';
import { SanitizeHtmlPipePipe } from './pipes/sanitize-html-pipe.pipe';
import { ProfilePictureComponent } from './components/profile-picture/profile-picture.component';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    MainPageComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    NewsItemComponent,
    UserListComponent,
    ForbiddenPageComponent,
    CoursespageComponent,
    CourseComponent,
    AddCourseComponent,
    CourseUserListComponent,
    SidenavListComponent,
    CourseMainPageComponent,
    CourseFilesComponent,
    SafeUrlPipe,
    QuizListComponent,
    QuizAddComponent,
    QuestionComponent,
    SchoolComponent,
    UserListDialog,
    UserComponent,
    SchoolDataComponent,
    QuizzViewComponent,
    SubmittedQuizzesComponent,
    NewPageComponent,
    SanitizeHtmlPipePipe,
    ProfilePictureComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatDialogModule,
    RoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    QuillModule.forRoot(),
    ToastrModule.forRoot({
        // positionClass: 'toast-top-center',
        // preventDuplicates: true,z
        closeButton: true,
        timeOut: 3000,
    }),
    CommonModule,
    MatInputModule,
    MatTableModule,
    JwtModule,
    NgbModule,
    NgbPopoverModule,
    MatCardModule,
    MatExpansionModule,
    MatCheckboxModule,
    FormsModule,
    ClipboardModule,
    MatSidenavModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule,
    MatPaginatorModule,
  ],
  providers: [
    {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
