import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APIRoutes } from '../app.routes';
import { CourseUser } from 'src/models/User';
import { Attachment, Course } from 'src/models/Course';
import { AuthService } from './auth.service';
import { UserRole } from 'src/models/enums/Enums';

@Injectable({
    providedIn: 'root'
})
export class CourseService {
    private api = APIRoutes.Courses;
    lastCourseId: string = '';
    public isTeacher: boolean = false;
    public isStudent: boolean = false;

    constructor(
        private http: HttpClient,
        // private authService: AuthService,
    ) { }
    getAllCourses(): Observable<Course[]> {
        return this.http.get<Course[]>(this.api);
    }

    getCourse(id: string): Observable<Course> {
        this.lastCourseId = id;
        return this.http.get<Course>(`${this.api}/${id}`);
    }

    getUsersNotInCourse(id: string){
        return this.http.get<CourseUser[]>(`${this.api}/${id}/usersNotInCourse`);
    }
    
    getUsers(id: string): Observable<CourseUser[]> {
        return this.http.get<CourseUser[]>(`${this.api}/${id}/users`);
    }

    createCourse(course: Course): Observable<Course> {
        return this.http.post<Course>(this.api, course); 
    }

    addUsers(id: string, users: CourseUser[]): Observable<any> {
        const students = users.filter(u => u.userRole === 'Student');
        const teachers = users.filter(u => u.userRole === 'Teacher');
        return this.http.post<any>(`${this.api}/${id}/addUsers`, {students, teachers});
    }

    updateCourse(course: Course): Observable<Course> {
        return this.http.put<Course>(`${this.api}/${course._id}`, course); 
    }

    deleteCourse(course: Course): Observable<Course> {
        return this.http.delete<Course>(`${this.api}/${course._id}`);
    }

    deleteUsers(id:string, users:string[]): Observable<any> {
        return this.http.delete<any>(`${this.api}/${id}/users`, { body: users });
    }

    uploadAttachment(attachment: FormData): Observable<Attachment> {
        return this.http.post<any>(`${this.api}/${this.lastCourseId}/uploadAttachment`, attachment);
    }

    getAttachments(): Observable<Attachment[]> {
        return this.http.get<Attachment[]>(`${this.api}/${this.lastCourseId}/attachments`);
    }

    deleteAttachment(id: string): Observable<string> {
        return this.http.delete<string>(`${this.api}/${this.lastCourseId}/attachments/${id}`);
    }

    getAttachmentContent(id: string): Observable<string> {
        return this.http.get<string>(`${this.api}/${this.lastCourseId}/attachments/${id}`);
    }

    downloadAttachment(id: string): Observable<any> {
        return this.http.get(`${this.api}/${this.lastCourseId}/attachments/${id}/download`,
            {responseType: 'blob'});
    }

    getCourseRole(course: Course, userId: string): Observable<UserRole> {
        return this.http.get<UserRole>(`${this.api}/${this.lastCourseId}/userRole/${userId}`);
    }
}
