import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIRoutes } from '../app.routes';
import { User } from 'src/models/User';
import { School } from 'src/models/School';


@Injectable({
  providedIn: 'root'
})
export class SchoolService {
    private api: string = APIRoutes.Schools;

    constructor(private http: HttpClient) { }

    getSchool(id:string): Observable<any> {
        return this.http.get<any>(`${this.api}/${id}`);
    }

    addSchool(user:User, school: School): Observable<any> {
        return this.http.post<any>(this.api, {
            userName: user.name,
            email: user.email,
            password: user.password,
            schoolName: school.schoolName,
        });
    }

    updateSchool(school: School): Observable<School> {
        return this.http.put<School>(`${this.api}/${school._id}`, school);
    }

    getSchoolByUrl(url: string): Observable<School> {
        return this.http.get<School>(this.api,
            { params: new HttpParams().set('url', url)});
    }

    uploadEmblem(emblemBase64: string, schoolId: string): Observable<string> {
        return this.http.post<any>(`${this.api}/${schoolId}/emblem`, {emblemBase64});
    }
}
