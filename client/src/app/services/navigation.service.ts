import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppRoutes } from '../app.routes';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class NavigationService {
    private visitedUrls: string[] = [];
    constructor(
        private router: Router,
        private authService: AuthService,
    ) { }

    goPrevPage(): void {
        if (this.visitedUrls.length > 1){
            this.router.navigate([this.visitedUrls[this.visitedUrls.length-2]]);
            this.visitedUrls.pop();
            console.log(this.visitedUrls);
        }else{
            const schoolUrl = this.authService.getSchoolUrl();
            this.navigate(`${schoolUrl}/${AppRoutes.main}`);
        }
    }

    navigate(url: string): void {
        url = '/' + url;
        console.log('erkezett', url);
        console.log('volt', this.router.url);
        
        if (this.router.url === url){
            console.info('NO NAVIGATING');
            return; 
        }
        if (this.visitedUrls[this.visitedUrls.length-1] !== url){
            this.visitedUrls.push(url);
        }
        this.router.navigate([url]);
        console.log(this.visitedUrls);
    }

    navigateWithBase(urlSegment: string, data: any = {}): void {
        const url = this.router.url + '/' + urlSegment;
        this.router.navigate([url, data]);
    }

    navigateWithoutSegment(segment: string): void {
        const url = this.router.url.split(segment);
        if (url.some(s => s === '')){
            this.router.navigate([url[0]]);
        } else {
            this.router.navigate(url);
        }
    }

    deleteLast(): void {
        this.visitedUrls.pop();
    }

    navigateWithNoSave(url: string): void {
        this.router.navigate([url]);
        console.log(this.visitedUrls);
    }

    getComponentName(): string {
        const result = this.visitedUrls[this.visitedUrls.length-1];
        if (result === '')
            return 'üres?';
        return result;
    }
}
