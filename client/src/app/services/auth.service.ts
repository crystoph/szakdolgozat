import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { APIRoutes } from '../app.routes';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { emptyUser, User } from 'src/models/User';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(
        private http: HttpClient,
    ) { }
    
    private helper = new JwtHelperService();
    private token: string | undefined;
    private refreshToken: string | undefined;
    public loggedInUser = new BehaviorSubject<User | undefined>(undefined);
    public schoolName = new BehaviorSubject<string | null>(null);
    public schoolUrl = new BehaviorSubject<string | null>(null);
    public schoolId = new BehaviorSubject<string | null>(null);

    login(email: string, password: string): Observable<any>{
        return this.http.post<any>(APIRoutes.Login, {email, password});
    }

    logout(): Observable<any> {
        sessionStorage.removeItem('login_token');
        sessionStorage.removeItem('schoolId');
        sessionStorage.removeItem('schoolName');
        sessionStorage.removeItem('refresh_token');
        sessionStorage.removeItem('schoolUrl');
        this.saveSchoolName(null);
        this.schoolUrl.next(null);
        this.schoolId.next(null);
        return this.http.delete<any>(APIRoutes.Logout);
    }

    saveTokens(accessToken: string, refreshToken: string, user:User ) {
        this.saveSchoolName(user.schoolName);
        this.saveSchoolUrl(user.schoolUrl);
        this.saveSchoolId(user.schoolId);
        this.loggedInUser.next(user);
        sessionStorage.setItem('login_token', accessToken);
        sessionStorage.setItem('refresh_token', refreshToken);
        sessionStorage.setItem('schoolId', user.schoolId);
        sessionStorage.setItem('schoolName', user.schoolName);
        sessionStorage.setItem('schoolUrl', user.schoolUrl);
    }

    saveSchoolName(schoolName: string | null) {
        this.schoolName.next(schoolName);
    }
    saveSchoolUrl(value: string){
        this.schoolUrl.next(value);
    }
    saveSchoolId(value: string){
        this.schoolId.next(value);
    }
    saveAccesToken(accessToken: string){
        sessionStorage.setItem('login_token', accessToken);
    }
    saveRefreshToken(refreshToken: string){
        sessionStorage.setItem('refresh_token', refreshToken);
    }

    isLoggedIn(): boolean {
        if (!sessionStorage.getItem('login_token')){
            return false;
        }else{
            return !this.helper.isTokenExpired(this.getToken());
        }
    }

    getToken(): string | undefined {
        const item = sessionStorage.getItem('login_token');
        if(item)
            return item;
        else
            return undefined;   
    }

    getRefreshToken(): string | null {
        return sessionStorage.getItem('refresh_token');
    }
    getAccessToken(): string | null {
        return sessionStorage.getItem('login_token');
    }

    isAdmin(): boolean {
        if (this.isLoggedIn()){         
            return this.getDecodedToken().userType === 'ADMIN';
        }
        return false;
    }

    getDecodedToken() {
        return this.helper.decodeToken(this.getToken());
    }

    getUserId(): string {
        return this.getDecodedToken()._id;
    }

    getUserName() : string {
        const decoded = this.getDecodedToken();
        return decoded?.name || 'nincs belépett felhasználó';
    }

    getSchoolId(): string | null {
        return this.schoolId.value ?? sessionStorage.getItem('schoolId');
        // return sessionStorage.getItem('schoolId') ?? undefined;
    }

    getSchoolName(): string | null {
        return this.schoolName.value ?? sessionStorage.getItem('schoolName');
        // return this.schoolName.value ?? sessionStorage.getItem('schoolName') ?? environment.appName;
    }
    getSchoolUrl(): string | null {
        return this.schoolUrl.value ?? sessionStorage.getItem('schoolUrl');
        // return sessionStorage.getItem('schoolUrl');
    }
    getUserProfilePicture(): string | null{
        if (this.loggedInUser.value){
            return this.loggedInUser.value.profilePicture;
        }
        return null;
    }

    setUserProfilePicture(base64: string){
        const user = this.loggedInUser.getValue();
        if (!user){
            return;
        }
        user.profilePicture = base64;
        this.loggedInUser.next(user);
    }

    refreshAccessToken(refreshToken: string): Observable<any> {
        return this.http.post<any>(APIRoutes.RefreshToken, { refreshToken });
    }
}
