import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { News } from 'src/models/New';
import { APIRoutes } from '../app.routes';

@Injectable({
    providedIn: 'root'
})
export class NewService {
    private api: string = APIRoutes.News;

    constructor(
        private http: HttpClient,
    ) {}

    getAllNews(schoolUrl: string): Observable<News[]> {
        return this.http.get<News[]>(this.api, 
            {params: new HttpParams().set('schoolId', schoolUrl)});
    }

    getNews(id: string, schoolUrl: string): Observable<News[]> {
        const params = new HttpParams().set('schoolId', schoolUrl).set('id', id);
        return this.http.get<News[]>(this.api, {params});
    }

    createNews(news: News): Observable<News> {
        return this.http.post<News>(this.api, news); 
    }

    updateNews(news: News): Observable<News> {
        return this.http.put<News>(`${this.api}/${news._id}`, news); 
    }

    publishNews(news: News, isPublic: boolean): Observable<News> {
        return this.http.put<News>(`${this.api}/${news._id}/publish`, {isPublic}); 
    }

    deleteNews(news: News): Observable<News> {
        return this.http.delete<News>(`${this.api}/${news._id}`);
    }
}
