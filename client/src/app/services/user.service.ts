import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { APIRoutes } from '../app.routes';
import { MainService } from './main.service';
import { User } from 'src/models/User';
import { NoDataResponse } from 'src/models/ServerResponses';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private apiUrl: string = APIRoutes.Users;
    // private schoolId: string = '';

    constructor(
        private http: HttpClient,
        private authService: AuthService,
    ) {
        // this.schoolId = this.authService.getSchoolId() ?? '';
    }

    getUsers(schoolId: string): Observable<any> {
        return this.http.get<any>(this.apiUrl,
            { params: new HttpParams().set('schoolId', schoolId) }
        );
    }

    getUser(id: string): Observable<User> {
        return this.http.get<User>(`${this.apiUrl}/${id}`);
    }

    addUser(user: User): Observable<any> {
        return this.http.post<User>(this.apiUrl, user);
    }

    updateUser(user: User): Observable<User> {
        return this.http.put<User>(`${this.apiUrl}/${user._id}`, 
            { ...user, userName: user.name} ); 
    }

    deleteUser(user: User): Observable<User> {
        return this.http.delete<User>(`${this.apiUrl}/${user._id}`);
    }

    generateUsers(numberOfUsers: number, schoolId: string): Observable<User[]> {
        return this.http.patch<User[]>(this.apiUrl+'/generate', { numberOfUsers, schoolId });
    }

    changePassword(user: User): Observable<NoDataResponse> {
        return this.http.put<NoDataResponse>(`${this.apiUrl}/${user._id}/ChangePassword`, { password: user.password });
    }

    getDefaultPassword(user: User): Observable<string | null> {
        return this.http.get<string | null>(`${this.apiUrl}/${user._id}/defaultPassword`);
    }

    uploadProfile(profileBase64: string, userId: string): Observable<string> {
        return this.http.post<any>(`${this.apiUrl}/${userId}/profile`, {profileBase64});
    }
}
