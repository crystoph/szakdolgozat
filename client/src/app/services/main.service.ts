import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class MainService {
    private allowedToEdit: boolean = false;
    private editMode: boolean = false;
    
    constructor(
        private authService: AuthService,
    ) { }

    toggleMode(): void {
        console.log(`Mode toggled: ${this.editMode} -> ${!this.editMode}`)
        this.editMode = !this.editMode;
    }

    isEditMode(): boolean {
        return this.editMode;
    }

    setAllowedToEdit(value: boolean){
        this.allowedToEdit = value;
    }

    getAllowedToEdit(): boolean {
        return this.authService.isAdmin() || this.allowedToEdit;
    }
}
