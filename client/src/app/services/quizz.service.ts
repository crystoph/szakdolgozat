import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Question, Quiz, QuizListItem } from 'src/models/Quiz';
import { APIRoutes } from '../app.routes';
import { CourseService } from './course.service';

@Injectable({
  providedIn: 'root'
})
export class QuizzService {
    public lastQuizId: string = '';
    public submittedOf: string = '';
    private api: string = APIRoutes.Quizzes;

    constructor(
        private http: HttpClient,
        private courseService: CourseService,
    ) { }

    getAllQuiz(courseId: string): Observable<QuizListItem[]> {
        return this.http.get<QuizListItem[]>(this.api, 
            {params: new HttpParams().set('courseId', courseId)});
    }

    addQuiz(quiz: Quiz): Observable<Quiz> {
        return this.http.post<Quiz>(this.api, quiz);
    }

    getQuiz(id: string, userId: string): Observable<Quiz> {
        return this.http.get<Quiz>(this.api + '/' + id, 
            {params: new HttpParams().set('user', userId)});
    }

    deleteQuiz(quiz: Quiz): Observable<string> {
        return this.http.delete<any>(this.api + '/' + quiz._id, 
            { params: new HttpParams().set('courseId', this.courseService.lastCourseId) });
    }

    deleteQuizById(quizId: string): Observable<string> {
        return this.http.delete<any>(this.api + '/' + quizId, 
            { params: new HttpParams().set('courseId', this.courseService.lastCourseId) });
    }

    updateQuiz(quiz: Quiz): Observable<Quiz> {
        return this.http.put<Quiz>(this.api + '/' + quiz._id, quiz);
    }

    publicQuiz(quiz: Quiz, isPublic: boolean): Observable<Quiz> {
        return this.http.post<Quiz>(`${this.api}/${quiz._id}/public`, {isPublic});
    }

    submitQuiz(quiz: Quiz): Observable<Quiz> {
        return this.http.post<Quiz>(`${this.api}/${quiz._id}/submit`, quiz);
    }

    getSubmitted(quizId: string): Observable<any> {
        return this.http.get<any>(`${this.api}/${quizId}/submitted`);
    }

    startQuiz(quiz: string): Observable<Quiz> {
        return this.http.post<Quiz>(`${this.api}/${quiz}/start`, {});
    }

    getUserAnswer(quizId: string, userId: string): Observable<Quiz> {
        return this.http.get<Quiz>(`${this.api}/${quizId}/user/${userId}`);
    }

    closeQuiz(userAnswerId: string, questions: Question[]){
        return this.http.put<Quiz>(`${this.api}/${userAnswerId}/close`, {questions});
    }
}
